package math.matrixsolver;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.Deflater;
import java.util.zip.DeflaterOutputStream;
import java.util.zip.InflaterInputStream;


public class ByteCompressor {
    private ByteCompressor() {
    } // Prohibit instantiation

    public static byte[] compressBytes(byte[] bytes) throws IOException {
        ByteArrayOutputStream bStream = new ByteArrayOutputStream();
        Deflater d = new Deflater();
        DeflaterOutputStream dStream = new DeflaterOutputStream(bStream, d);
        dStream.write(bytes);
        dStream.close();
        return bStream.toByteArray();
    }

    public static byte[] decompressBytes(byte[] bytes) throws IOException {
        InflaterInputStream in = new InflaterInputStream(new ByteArrayInputStream(bytes));
        ByteArrayOutputStream bout = new ByteArrayOutputStream();
        int b;
        while ((b = in.read()) != -1)
            bout.write(b);
        in.close();
        return bout.toByteArray();
    }
}