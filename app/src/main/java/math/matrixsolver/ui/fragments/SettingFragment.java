package math.matrixsolver.ui.fragments;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import java.util.Calendar;
import java.util.Objects;

import math.matrixsolver.R;
import math.matrixsolver.ui.activities.RootActivity;
import math.matrixsolver.ui.tutorial.RunOnceManager;

public class SettingFragment extends PreferenceFragmentCompat {

    private void restartSelf(long duration) {
        AlarmManager am = (AlarmManager) Objects.requireNonNull(getActivity()).getSystemService(Context.ALARM_SERVICE);
        am.set(AlarmManager.RTC_WAKEUP, Calendar.getInstance().getTimeInMillis() + duration,
                PendingIntent.getActivity(getActivity(), 0, getActivity().getIntent(), PendingIntent.FLAG_ONE_SHOT
                        | PendingIntent.FLAG_CANCEL_CURRENT));
        Intent i = getActivity().getBaseContext().getPackageManager()
                .getLaunchIntentForPackage(getActivity().getBaseContext().getPackageName());
        assert i != null;
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }
    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        setPreferencesFromResource(R.xml.root_preferences, rootKey);
        ((Preference) Objects.requireNonNull(findPreference("showcase_again"))).setOnPreferenceClickListener(preference -> {
            new RunOnceManager(getContext(), RootActivity.SETTINGS_KEY_SHOWCASE).setIfNeedToRun(true);
            restartSelf(0);
            return true;
        });
    }
}