package math.matrixsolver.ui.fragments;


import android.content.ContentResolver;
import android.content.res.Resources;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.daquexian.flexiblerichtextview.FlexibleRichTextView;
import com.github.kiprobinson.bigfraction.BigFraction;

import org.scilab.forge.jlatexmath.core.AjLatexMath;

import java.math.RoundingMode;

import math.matrixsolver.R;
import math.matrixsolver.matrix.Matrix;
import math.matrixsolver.matrix.TeXConverter;
import math.matrixsolver.matrix.solver.methods.GaussSolver;
import math.matrixsolver.ui.activities.ActivityEssential;

public class ProblemFragment extends CorrectThemedFragment {
    private final BigFraction PERCENT_DIVISOR = BigFraction.valueOf(100);
    private int[] xmls = {R.layout.intro, R.layout.problem1, R.layout.problem2};
    private int xmlNumber;
    private String newline = "<br>";
    private EditText kg;
    private EditText[] percents = new EditText[3];
    private EditText[] lower = new EditText[2];

    public void setXmlNumber(int xmlNumber) {
        this.xmlNumber = xmlNumber;
    }

    public int getXmlsArrLength() {
        return xmls.length;
    }

    private ProblemFragment.ProblemCallback callback;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        AjLatexMath.init(getContext());
        if (ActivityEssential.isThemeDark(getActivity())) AjLatexMath.setColor(Color.WHITE);
        if (xmlNumber == 0) {
            Resources resources = getResources();
            int[] resIds = {R.drawable.eq1a, R.drawable.eq2, R.drawable.eq3};
            FlexibleRichTextView introView = view.findViewById(R.id.introView);
            String content = resources.getString(R.string.intro);
            Uri uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + resources.getResourcePackageName(resIds[0]) + '/' + resources.getResourceTypeName(resIds[0]) + '/' + resources.getResourceEntryName(resIds[0]));
            content = content.replace("!EQ1!", "[img]" + uri + "[/img]");
            uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + resources.getResourcePackageName(resIds[1]) + '/' + resources.getResourceTypeName(resIds[1]) + '/' + resources.getResourceEntryName(resIds[1]));
            content = content.replace("!EQ2!", "[img]" + uri + "[/img]");
            uri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + resources.getResourcePackageName(resIds[2]) + '/' + resources.getResourceTypeName(resIds[2]) + '/' + resources.getResourceEntryName(resIds[2]));
            content = content.replace("!EQ3!", "[img]" + uri + "[/img]");
            introView.setText(content);
        } else if (xmlNumber == 1) {
            kg = view.findViewById(R.id.kg);
            percents[0] = view.findViewById(R.id.percent1);
            percents[1] = view.findViewById(R.id.percent2);
            percents[2] = view.findViewById(R.id.percent3);
            lower[0] = view.findViewById(R.id.lower1);
            lower[1] = view.findViewById(R.id.lower2);
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(xmls[xmlNumber], container, false);
    }

    public void solveProblem() {
        StringBuilder sb = new StringBuilder();

        try {
            if (xmlNumber == 1) {
                BigFraction[] percentNs = new BigFraction[percents.length];
                BigFraction kgN;
                BigFraction[] lowerNs = new BigFraction[lower.length];
                kgN = new BigFraction(kg.getText().toString());
                for (int i = 0; i < lowerNs.length; i++)
                    lowerNs[i] = new BigFraction(lower[i].getText().toString());
                for (int i = 0; i < percentNs.length; i++)
                    percentNs[i] = new BigFraction(percents[i].getText().toString());
                BigFraction total_percents = BigFraction.ZERO;
                for (BigFraction perc : percentNs) {
                    if (perc.compareTo(BigFraction.ZERO) < 0) throw new Exception();
                    total_percents = total_percents.add(perc);
                }
                if (total_percents.compareTo(PERCENT_DIVISOR) > 0) throw new Exception();

                for (int i = 0; i < percentNs.length; i++) {
                    percentNs[i] = percentNs[i].divide(PERCENT_DIVISOR);
                    sb.append("$").append(percents[i].getText()).append("\\% = ").append(TeXConverter.BigFractionToTeX(percentNs[i])).append("$").append(System.lineSeparator());
                }
                sb.append("Составим СЛАУ:").append(newline);

                BigFraction[][] matrixArray = {{BigFraction.ONE, BigFraction.ONE, BigFraction.ONE, kgN},
                        {percentNs[0], percentNs[1].negate(), percentNs[2].negate(), lowerNs[0]},
                        {new BigFraction("0.000000000001"), percentNs[1], percentNs[2].negate(), lowerNs[1]}};
                Matrix matrix = new Matrix(matrixArray);
                sb.append(TeXConverter.SLAEToTeX(TeXConverter.matrixToSLAE(matrix)));

                GaussSolver gaussSolver = new GaussSolver(getContext());
                BigFraction[] roots = gaussSolver.solveMatrix(matrix);
                sb.append(gaussSolver.getSolvingLog());
                sb.append("Ответ: ").append(newline);
                for (int i = 0; i < roots.length; i++)
                    sb.append("Масса ").append(i + 1).append("-го куска сплава равна ")
                            .append(TeXConverter.BigFractionToBigDecimal(roots[i], 3, RoundingMode.HALF_UP).toPlainString()).append(" кг").append(newline);
            } else if (xmlNumber == 2) {
                sb.append("Составим СЛАУ:").append(newline);

                BigFraction[][] matrixArray = {{new BigFraction(2), new BigFraction(7), new BigFraction(3), new BigFraction(49)},
                        {new BigFraction(4), new BigFraction(3), new BigFraction(2), new BigFraction(56)},
                        {new BigFraction(5), new BigFraction(6), new BigFraction(10), new BigFraction(112)}};
                Matrix matrix = new Matrix(matrixArray);
                sb.append(TeXConverter.SLAEToTeX(TeXConverter.matrixToSLAE(matrix)));

                GaussSolver gaussSolver = new GaussSolver(getContext());
                BigFraction[] roots = gaussSolver.solveMatrix(matrix);
                sb.append(gaussSolver.getSolvingLog());
                sb.append("Ответ: ").append(newline);
                String[] objects = {"карандаша", "ручки", "тетради"};
                for (int i = 0; i < roots.length; i++)
                    sb.append("Цена ").append(objects[i]).append(" равна ")
                            .append(TeXConverter.BigFractionToBigDecimal(roots[i], 3, RoundingMode.HALF_UP).toPlainString()).append(" руб.").append(newline);
            }
        } catch (Exception e) {
            Toast.makeText(getContext(), R.string.corrupted_problem, Toast.LENGTH_SHORT).show();
            return;
        }

        if (callback != null) callback.onProblemSolved(sb.toString());

    }

    public void registerCallBack(ProblemFragment.ProblemCallback callback) {
        this.callback = callback;
    }

    public void unregisterCallBack() {
        this.callback = null;
    }

    public interface ProblemCallback {
        void onProblemSolved(String solving);
    }

}