package math.matrixsolver.ui.fragments;

import android.Manifest;
import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import math.matrixsolver.PermissionManager;
import math.matrixsolver.R;
import math.matrixsolver.ui.activities.TransactionManager;
import math.matrixsolver.ui.fragments.matrix_list.MatrixListFragment;

public class ToolbarFragment extends CorrectThemedFragment implements View.OnClickListener, Toolbar.OnMenuItemClickListener {
    private static final String TITLE = "Title";
    private static final String CHILD_ARGUMENTS = "arguments";

    private String mName = null;

    public static ToolbarFragment newInstance(@StringRes int titleResId) {
        ToolbarFragment fragment = new ToolbarFragment();
        Bundle args = new Bundle();
        args.putInt(TITLE, titleResId);
        fragment.setArguments(args);
        return fragment;
    }

    public static ToolbarFragment newInstance(@StringRes int titleResId, @Nullable Bundle bundle) {
        ToolbarFragment fragment = new ToolbarFragment();
        Bundle args = new Bundle();
        args.putInt(TITLE, titleResId);
        args.putBundle(CHILD_ARGUMENTS, bundle);
        fragment.setArguments(args);
        return fragment;
    }

    private void startQrScan() {
        Activity activity = getActivity();
        if (!(activity instanceof TransactionManager)) return;
        ((TransactionManager) activity).showQrScanning();
    }

    private void startLoadingFromFile() {
        Activity activity = getActivity();
        if (!(activity instanceof TransactionManager)) return;
        ((TransactionManager) activity).startLoadingFromFile();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        setRetainInstance(true);
        return inflater.inflate(R.layout.fragment_toolbar, container, false);
    }

    AnswerFragment answerFragment = null;

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Bundle arguments = getArguments();
        if (arguments == null) return;
        Toolbar toolbar = view.findViewById(R.id.toolbar);
        int titleResId = arguments.getInt(TITLE);
        Bundle bundle = arguments.getBundle(CHILD_ARGUMENTS);
        toolbar.setTitle(titleResId);
        toolbar.setNavigationOnClickListener(this);
        toolbar.setOnMenuItemClickListener(this);
        if (titleResId == R.string.filemanager) toolbar.inflateMenu(R.menu.filemanager);
        if (titleResId == R.string.answer) toolbar.inflateMenu(R.menu.answer);

        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager == null || savedInstanceState != null) return;
        Fragment fragment = getFragment(titleResId, bundle);
        assert fragment != null;
        mName = fragment.getClass().getName() + System.currentTimeMillis();
        fragmentManager.beginTransaction().add(
                R.id.container,
                fragment,
                mName
        ).commit();
    }

    private Fragment getFragment(int titleResId, @Nullable Bundle bundle) {
        switch (titleResId) {
            case R.string.answer:
                answerFragment = AnswerFragment.newInstance(bundle);
                return answerFragment;
            case R.string.filemanager:
                return MatrixListFragment.newInstance(bundle);
            case R.string.settings:
                return new SettingFragment();
            case R.string.scan_qr:
                return new QrCodeScanningFragment();
            case R.string.about:
                return new AboutFragment();
            default:
                return null;
        }
    }

    @Override
    public void onClick(View v) {
        Activity activity = getActivity();
        if (!(activity instanceof TransactionManager)) return;
        ((TransactionManager) activity).goBack();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        FragmentManager fragmentManager = getFragmentManager();
        if (fragmentManager == null || mName == null) return;
        Fragment fragment = fragmentManager.findFragmentByTag(mName);
        assert fragment != null;
        fragmentManager.beginTransaction().remove(fragment).commitAllowingStateLoss();
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        Activity activity = getActivity();
        if (activity == null) return false;
        switch (item.getItemId()) {
            case R.id.qr:
                if (PermissionManager.checkIfNeedToRequestPermission(activity, Manifest.permission.CAMERA)
                        == PermissionManager.NEED_TO_ASK_FOR_PERMISSION) {
                Toast.makeText(activity, R.string.permissions_camera_denied, Toast.LENGTH_SHORT).show();
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.CAMERA}, 100);
                return false;
            }
                startQrScan();
                return true;
            case R.id.file:
                if (PermissionManager.checkIfNeedToRequestPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PermissionManager.NEED_TO_ASK_FOR_PERMISSION
                        && PermissionManager.checkIfNeedToRequestPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) == PermissionManager.NEED_TO_ASK_FOR_PERMISSION) {
                    Toast.makeText(activity, R.string.permissions_external_storage_description, Toast.LENGTH_SHORT).show();
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 100);
                    return false;
                }
                startLoadingFromFile();
                return true;
            case R.id.detailed_solution:
                if (answerFragment != null) {
                    answerFragment.showStepsOfSolution();
                }
                return true;
        }
        return false;
    }
}