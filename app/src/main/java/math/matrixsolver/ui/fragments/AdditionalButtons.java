package math.matrixsolver.ui.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import math.matrixsolver.R;
import math.matrixsolver.ui.activities.ActivityEssential;

public class AdditionalButtons extends CorrectThemedFragment implements View.OnClickListener {

    private NormalAdditionalButtonsEvent mToolbarListener = null;

    public void addToolbarListener(NormalAdditionalButtonsEvent event) {
        mToolbarListener = event;
    }

    public void removeToolbarListener() {
        mToolbarListener = null;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.additional_buttons_normal, container, false);
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final ArrayList<View> imgBtns = ActivityEssential.findAllViews((ViewGroup) view, ImageButton.class);
        ActivityEssential.addOnClickListener(imgBtns, this);
    }

    @Override
    public void onClick(View v) {
        if (mToolbarListener == null) return;
        mToolbarListener.onKeyAction(v.getId());
    }

    public interface NormalAdditionalButtonsEvent {
        int ACTION_GENERATE_MATRIX = R.id.randomGenerationButton;
        int ACTION_ERASE_MATRIX = R.id.eraseMatrixButton;

        void onKeyAction(int actionType);
    }

}