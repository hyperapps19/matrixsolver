package math.matrixsolver.ui.fragments;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.Objects;

import katex.hourglass.in.mathlib.MathView;

import static math.matrixsolver.ui.activities.ActivityEssential.setupMathView;

public class AnswerFragment extends Fragment {

    private MathView mMathView;

    public static AnswerFragment newInstance(Bundle bundle) {
        AnswerFragment fragment = new AnswerFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mMathView = new MathView(inflater.getContext());
        mMathView.setClickable(false);
        setupMathView(Objects.requireNonNull(getActivity()), mMathView);
        Bundle args = getArguments();
        if (args != null) mMathView.setDisplayText(args.getString("Answer"));
        return mMathView;
    }

    public void showStepsOfSolution() {
        Bundle args = getArguments();
        if (args != null)
            mMathView.setDisplayText(args.getString("Answer") + args.getString("DetailedSolution"));
    }
}
