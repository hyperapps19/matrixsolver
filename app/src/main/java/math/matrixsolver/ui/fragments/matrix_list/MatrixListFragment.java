package math.matrixsolver.ui.fragments.matrix_list;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.os.Bundle;
import android.text.InputType;
import android.util.Base64;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.github.kiprobinson.bigfraction.BigFraction;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.gson.Gson;

import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

import math.matrixsolver.ByteCompressor;
import math.matrixsolver.R;
import math.matrixsolver.data.ItemMatrixData;
import math.matrixsolver.db.DBHelper;
import math.matrixsolver.ui.activities.QRShowActivity;
import math.matrixsolver.ui.activities.TransactionManager;
import math.matrixsolver.ui.fragments.KeyboardFragment;

public class MatrixListFragment extends Fragment implements OnItemListener {
    private SavedMatrixAdapter adapter;

    public static MatrixListFragment newInstance(Bundle bundle) {
        MatrixListFragment fragment = new MatrixListFragment();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_matrix_list, container, false);
    }

    public static String matrixToJSON(BigFraction[][] matrix) {
        return new Gson().toJson(matrix);
    }

    public static byte[] getCompressedMatrix(BigFraction[][] matrix) {
        try {
            return ByteCompressor.compressBytes(matrixToJSON(matrix).getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new byte[0];
    }

    static BigFraction[][] matrixFromJSON(String JSON) {
        return new Gson().fromJson(JSON, BigFraction[][].class);
    }

    public static BigFraction[][] matrixFromBytes(byte[] bytes) {
        try {
            return matrixFromJSON(new String(ByteCompressor.decompressBytes(bytes)));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private void showSaveDialog() {
        Resources res = getResources();
        Context context = getContext();
        if (context == null) return;
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.matrix_save_title)
                .setMessage(R.string.matrix_save_message)
                .setPositiveButton(R.string.matrix_save_positive, (dialog, which) -> {
                    localSave();
                    dialog.dismiss();
                }).setNeutralButton(R.string.matrix_save_network, (dialog, which) -> {
            networkSave();
            dialog.dismiss();
        }).setNegativeButton(R.string.matrix_save_file, (dialog, which) -> {
            fileSave();
            dialog.dismiss();
        })
                .setCancelable(true)
                .create().show();
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        RecyclerView recyclerView = view.findViewById(R.id.recycler);
        adapter = new SavedMatrixAdapter(Objects.requireNonNull(getActivity()), getAvailableMatrix());
        adapter.setOnItemListener(this);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        FloatingActionButton button = view.findViewById(R.id.float_button);
        button.setOnClickListener(v -> showSaveDialog());
    }


    private void addItem(ItemMatrixData data) {
        adapter.addItem(data);
        adapter.notifyItemInserted(adapter.getItemCount());
    }

    private void deleteItem(int position) {
        adapter.deleteItem(position);
        adapter.notifyItemRemoved(position);
    }

    private void networkSave() {
        BigFraction[][] matrix = getMatrix();
        if (matrix == null) return;

        //TODO: здесь должно быть сохранение в сеть
        try {
            String url = new PastebinUploadTask().execute(matrix).get();
            if (url != null)
                saveMatrixToDB(matrix, false, url);
            else {
                Toast.makeText(this.getContext(), "Нет подключения к Интернету", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private SQLiteDatabase initDB() {
        DBHelper dbHelper = new DBHelper(getContext(), "MatrixDB", null, 4);
        return dbHelper.getWritableDatabase();
    }

    public void saveMatrixToDB(BigFraction[][] matrix, boolean isLocal, String URL) {
        SQLiteDatabase db = initDB();
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle(R.string.save_matrix_title);
        alertDialog.setMessage(R.string.save_matrix_msg);
        final EditText input = new EditText(getContext());
        input.setImeOptions(EditorInfo.IME_FLAG_NO_EXTRACT_UI);
        input.setMaxLines(1);
        input.setInputType(InputType.TYPE_CLASS_TEXT);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        input.setLayoutParams(lp);
        alertDialog.setView(input);
        alertDialog.setPositiveButton(R.string.showcase_dismiss, (dialog, which) -> {
            SQLiteStatement statement = db.compileStatement("INSERT INTO \"" + DBHelper.DBName + "\" VALUES (?,?,?,?,?)");
            statement.clearBindings();
            statement.bindString(1, input.getText().toString());
            statement.bindString(2, KeyboardFragment.formatMatrixSize(matrix[0].length));
            statement.bindLong(3, System.currentTimeMillis());
            if (isLocal) statement.bindBlob(4, getCompressedMatrix(matrix));
            else {
                statement.bindString(5, URL);
                Intent intent = new Intent(getActivity(), QRShowActivity.class);
                intent.putExtra("qrData", URL);
                intent.putExtra("isLink", true);
                startActivity(intent);
            }
            statement.executeInsert();
            addItem(getItemDataAtLast());
        });
        alertDialog.create().show();
    }

    private void localSave() {
        BigFraction[][] matrix = getMatrix();
        if (matrix == null) return;
        //TODO: здесь должно быть сохранение в устройство
        saveMatrixToDB(matrix, true, null);
    }

    private void fileSave() {
        BigFraction[][] matrix = getMatrix();
        if (matrix == null) return;
        //TODO: здесь должно быть сохранение в файл
        Activity activity = getActivity();
        if (!(activity instanceof TransactionManager)) return;
        ((TransactionManager) activity).startSavingToFile();
    }

    private List<ItemMatrixData> getAvailableMatrix() {
        //TODO: здесь должен быть список данных

        List<ItemMatrixData> data = new ArrayList<>();

        SQLiteDatabase db = initDB();
        Cursor c = db.rawQuery("SELECT rowid, Name, Size, CreationDate, URL FROM " + DBHelper.DBName, null);
        if (c.moveToFirst()) { // We have some data here
            do
                data.add(new ItemMatrixData(c.getInt(0), c.getString(1), c.getString(2), DateFormat.getDateTimeInstance().format(new Date(c.getLong(3))), c.getString(4) == null));
            while (c.moveToNext());
        }
        c.close();

        return data;
    }

    private ItemMatrixData getItemDataAtLast() {
        SQLiteDatabase db = initDB();
        ItemMatrixData itemMatrixData = null;
        Cursor c = db.rawQuery("SELECT rowid, Name, Size, CreationDate, URL FROM " + DBHelper.DBName, null);
        if (c.moveToLast()) { // We have some data here
            itemMatrixData = new ItemMatrixData(c.getInt(0), c.getString(1), c.getString(2), DateFormat.getDateTimeInstance().format(new Date(c.getLong(3))), c.getString(4) == null);
        }
        c.close();

        return itemMatrixData;
    }

    public static BigFraction[][] matrixFromPastebin(Context context, String pasteKey) {
        String s = null;
        try {
            s = new PastebinGetTask().execute(pasteKey).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        if (s == null) return null;
        BigFraction[][] matrix;
        matrix = matrixFromBytes(Base64.decode(s, Base64.DEFAULT));
        return matrix;
    }

    @Override
    public void onClick(ItemMatrixData item) {
        Activity activity = getActivity();
        if (!(activity instanceof TransactionManager)) return;
        ((TransactionManager) activity).loadMatrix(loadMatrix(item));
        //TODO: здесь необходимо загрузить данные матрицы
    }

    private BigFraction[][] getMatrix() {
        Bundle args = getArguments();
        if (args == null) return null;
        BigFraction[][] matrix = new BigFraction[args.getInt("Length")][];
        for (int i = 0; i < matrix.length; i++) {
            String[] array = args.getStringArray(String.format(Locale.ENGLISH, "Matrix%d", i));
            if (array == null) continue;
            matrix[i] = new BigFraction[array.length];
            for (int j = 0; j < array.length; j++) {
                matrix[i][j] = new BigFraction(array[j]);
            }
        }
        return matrix;
    }

    private BigFraction[][] loadMatrix(ItemMatrixData item) {
        //TODO: здесь необходимо загрузить данные матрицы
        BigFraction[][] matrix = new BigFraction[0][];
        SQLiteDatabase db = initDB();
        Cursor c = db.rawQuery("SELECT Data, URL FROM " + DBHelper.DBName + " WHERE rowid=" + item.getRowId(), null);
        if (c != null && c.moveToFirst()) {
            if (c.getBlob(0) != null) matrix = matrixFromBytes(c.getBlob(0)); //Local matrix
            else matrix = matrixFromPastebin(getContext(), c.getString(1));
            c.close();
        }
        db.close();
        return matrix;
    }
}