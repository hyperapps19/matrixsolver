package math.matrixsolver.ui.fragments.matrix_list;

import android.os.AsyncTask;
import android.util.Base64;

import com.github.kiprobinson.bigfraction.BigFraction;

import org.jpaste.exceptions.PasteException;
import org.jpaste.pastebin.Pastebin;

import java.security.InvalidParameterException;

class PastebinUploadTask extends AsyncTask<BigFraction[][], Void, String> {

    final static String PASTEBIN_DEV_KEY = "538f17288780966d0419866f318afe13";
    final static String PASTEBIN_TITLE = "Paste from MatrixSolver app";

    PastebinUploadTask() {
    }

    @Override
    protected String doInBackground(BigFraction[][]... bigFractions) {
        if (bigFractions.length != 1) throw new InvalidParameterException();
        try {
            return Pastebin.newPaste(PASTEBIN_DEV_KEY, new String(Base64.encode(MatrixListFragment.getCompressedMatrix(bigFractions[0]), Base64.DEFAULT)), PASTEBIN_TITLE)
                    .paste().getKey();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}

class PastebinGetTask extends AsyncTask<String, Void, String> {

    PastebinGetTask() {
    }

    @Override
    protected String doInBackground(String... strings) {
        if (strings.length != 1) throw new InvalidParameterException();
        try {
            return Pastebin.getContents(strings[0]);
        } catch (Exception e) {
            return null;
        }
    }
}