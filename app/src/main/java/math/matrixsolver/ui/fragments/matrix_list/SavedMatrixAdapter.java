package math.matrixsolver.ui.fragments.matrix_list;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import math.matrixsolver.R;
import math.matrixsolver.data.ItemMatrixData;
import math.matrixsolver.ui.activities.ActivityEssential;

public class SavedMatrixAdapter extends RecyclerView.Adapter<SavedMatrixAdapter.ViewHolder> {

    private List<ItemMatrixData> mData;
    private LayoutInflater mInflater;
    private OnItemListener mListener = null;
    private Activity mActivity;

    public SavedMatrixAdapter(Activity activity, List<ItemMatrixData> data) {
        mInflater = LayoutInflater.from(activity.getBaseContext());
        mData = data;
        mActivity = activity;
    }

    public void setOnItemListener(OnItemListener listener) {
        mListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.recyclerview_entry, parent, false);
        view.setLayoutParams(new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(view, mActivity);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(mData.get(position));
    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    public void addItem(ItemMatrixData data) {
        this.mData.add(data);
    }

    public void deleteItem(int position) {
        this.mData.remove(position);
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        private ImageView mIconView;
        private TextView mTextView;
        private View mView;
        private Activity mActivity;

        ViewHolder(@NonNull View view, Activity activity) {
            super(view);
            mView = view;
            mActivity = activity;
        }

        public void bind(ItemMatrixData data) {
            mIconView = mView.findViewById(R.id.entryIcon);
            mTextView = mView.findViewById(R.id.name);
            mTextView.setText(data.getName());
            if (data.isLocal()) {
                final Drawable drawable = ContextCompat.getDrawable(mActivity.getBaseContext(), R.drawable.ic_file);
                if (ActivityEssential.isThemeDark(mActivity)) {
                    assert drawable != null;
                    drawable.setTint(Color.WHITE);
                }
                mIconView.setImageDrawable(drawable);
            } else
                mIconView.setImageDrawable(ContextCompat.getDrawable(mActivity.getBaseContext(), R.drawable.ic_internet));
            ((TextView) mView.findViewById(R.id.size)).setText(data.getSize());
            ((TextView) mView.findViewById(R.id.date)).setText(data.getDate());
            mView.setOnClickListener(v -> {
                if (mListener != null) mListener.onClick(data);
            });
        }
    }
}
