package math.matrixsolver.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.github.kiprobinson.bigfraction.BigFraction;
import com.google.zxing.Result;

import math.matrixsolver.R;
import math.matrixsolver.ui.activities.RootActivity;
import math.matrixsolver.ui.activities.TransactionManager;
import math.matrixsolver.ui.fragments.matrix_list.MatrixListFragment;

public class QrCodeScanningFragment extends Fragment implements DecodeCallback {
    private CodeScanner mCodeScanner;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.activity_qr_scanning, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        CodeScannerView scannerView = view.findViewById(R.id.scanner_view);
        mCodeScanner = new CodeScanner(view.getContext(), scannerView);
        mCodeScanner.setDecodeCallback(this);
        scannerView.setOnClickListener(v -> mCodeScanner.startPreview());
    }

    @Override
    public void onResume() {
        super.onResume();
        mCodeScanner.startPreview();
    }

    @Override
    public void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }

    @Override
    public void onDecoded(@NonNull Result result) {
        //TODO: тут сделать сохранение
        final BigFraction[][] matrix = MatrixListFragment.matrixFromPastebin(getContext(), result.getText());
        final Activity activity = getActivity();
        if (!(activity instanceof TransactionManager)) return;
        activity.runOnUiThread(() -> {
            ((TransactionManager) activity).goBack();
            ((RootActivity) activity).loadMatrix(matrix);
        });
    }
}
