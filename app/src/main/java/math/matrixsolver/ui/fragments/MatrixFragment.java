package math.matrixsolver.ui.fragments;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.InputType;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.HorizontalScrollView;
import android.widget.ScrollView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.github.kiprobinson.bigfraction.BigFraction;

import math.matrixsolver.R;
import math.matrixsolver.ui.activities.ActivityEssential;

public class MatrixFragment extends Fragment implements View.OnFocusChangeListener {
    private int mHSize = 2;
    private GridLayout mGridLayout;
    private ScrollView mScrollView;
    private HorizontalScrollView mHScrollView;
    private EditText mFocusedEditText = null;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        mScrollView = new ScrollView(inflater.getContext());
        mScrollView.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        ));
        mScrollView.setOverScrollMode(ScrollView.OVER_SCROLL_IF_CONTENT_SCROLLS);
        mScrollView.setVerticalScrollBarEnabled(false);
        mScrollView.setHorizontalScrollBarEnabled(false);

        mHScrollView = new HorizontalScrollView(inflater.getContext());
        mHScrollView.setOverScrollMode(ScrollView.OVER_SCROLL_IF_CONTENT_SCROLLS);
        mHScrollView.setHorizontalScrollBarEnabled(false);
        mHScrollView.setFillViewport(true);
        FrameLayout.LayoutParams hlp = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        hlp.gravity = Gravity.CENTER;
        mHScrollView.setLayoutParams(hlp);
        mScrollView.addView(mHScrollView);

        mGridLayout = new GridLayout(inflater.getContext());
        FrameLayout.LayoutParams glp = new FrameLayout.LayoutParams(
                ViewGroup.LayoutParams.WRAP_CONTENT,
                ViewGroup.LayoutParams.WRAP_CONTENT
        );
        //glp.gravity = Gravity.CENTER;
        mGridLayout.setLayoutParams(glp);
        mGridLayout.requestFocus();
        mHScrollView.addView(mGridLayout);
        setMatrixSize(mHSize);
        return mScrollView;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    public BigFraction[][] getMatrix() {
        BigFraction[][] result = new BigFraction[mHSize - 1][mHSize];
        try {
            for (int i = 0; i < mGridLayout.getChildCount(); i++) {
                String text = ((EditText) mGridLayout.getChildAt(i)).getText().toString();
                if (text.isEmpty()) text = "0";
                result[i / mHSize][i % mHSize] = new BigFraction(text);
            }
        } catch (NumberFormatException e) { // Matrix parsing error
            Toast.makeText(getContext(), R.string.corrupted_matrix, Toast.LENGTH_SHORT).show();
            return null;
        }
        return result;
    }

    public int getMatrixSize() {
        return mHSize;
    }

    @SuppressLint("SetTextI18n")
    public void setMatrix(BigFraction[][] matrix) {
        setMatrixSize(matrix.length + 1);
        for (int i = 0; i < mGridLayout.getChildCount(); i++) {
            EditText v = (EditText) mGridLayout.getChildAt(i);
            v.setText(matrix[i / mHSize][i % mHSize].toString().replace("/1", ""));
        }
    }

    public void setMatrixSize(int horizontalSize) {
        int count = horizontalSize * (horizontalSize - 1);
        while (mGridLayout.getChildCount() < count) {
            mGridLayout.addView(createEmptyEditView(mGridLayout.getContext()));
        }
        if (mGridLayout.getChildCount() > count) {
            View[] view = new View[count];
            int iterator = 0;
            for (int i = mGridLayout.getChildCount() - 1; i >= 0; i--) {
                if (i / mHSize < (horizontalSize - 1) && i % mHSize < horizontalSize) {
                    view[iterator] = mGridLayout.getChildAt(i);
                    iterator++;
                }
                mGridLayout.removeViewAt(i);
            }
            mGridLayout.setColumnCount(horizontalSize);
            for (int i = view.length - 1; i >= 0; i--) {
                mGridLayout.addView(view[i]);
            }
        }
        mGridLayout.setColumnCount(horizontalSize);
        mHSize = horizontalSize;
        if (mScrollView != null && mHScrollView != null) {
            boolean isScrollingVertical = mScrollView.canScrollVertically(1) || mScrollView.canScrollVertically(-1);
            boolean isScrollingHorizontal = mHScrollView.canScrollHorizontally(1) || mHScrollView.canScrollHorizontally(-1);
            int flag = 0;
            if (!isScrollingVertical) flag += Gravity.CENTER_VERTICAL;
            if (!isScrollingHorizontal) flag += Gravity.CENTER_HORIZONTAL;
            setFlag(flag);
        }
    }

    public void setFlag(int flag) {
        ((FrameLayout.LayoutParams) mHScrollView.getLayoutParams()).gravity = flag;
    }

    public void inputNumber(int num) {
        if (mFocusedEditText == null) return;
        String text = mFocusedEditText.getText().toString();
        text = text.substring(0, mFocusedEditText.getSelectionStart()) + num;
        int newPosition = text.length();
        text += mFocusedEditText.getText().toString().substring(mFocusedEditText.getSelectionEnd());
        mFocusedEditText.setText(text);
        mFocusedEditText.setSelection(newPosition);
    }

    public void changeSign() {
        if (mFocusedEditText != null && mFocusedEditText.getText().length() > 0) {
            int positionStart = mFocusedEditText.getSelectionStart();
            int positionEnd = mFocusedEditText.getSelectionEnd();
            char symbol = mFocusedEditText.getText().charAt(0);
            if (symbol == '-') {
                mFocusedEditText.setText(mFocusedEditText.getText().toString().substring(1));
                //TODO: start incorrect
                mFocusedEditText.setSelection(
                        positionStart - 1 > 0 ? positionStart - 1 : positionStart,
                        positionEnd - 1 > 0 ? positionEnd - 1 : positionEnd
                );
            } else {
                mFocusedEditText.setText(String.format("-%s", mFocusedEditText.getText()));
                mFocusedEditText.setSelection(
                        positionStart + 1,
                        positionEnd + 1
                );
            }
        }
    }

    public void nextCell() {
        if (mFocusedEditText == null) return;
        int index = mGridLayout.indexOfChild(mFocusedEditText);
        mFocusedEditText = (EditText) mGridLayout.getChildAt(
                mGridLayout.getChildCount() == index + 1 ? 0 : index + 1
        );
        mFocusedEditText.requestFocus();
        mFocusedEditText.selectAll();
    }

    public void backspace() {
        if (mFocusedEditText == null) return;
        if (mFocusedEditText.getSelectionStart() == mFocusedEditText.getSelectionEnd()) {
            if (mFocusedEditText.getSelectionStart() == 0) return;
            int startPos = mFocusedEditText.getSelectionStart();
            String text = mFocusedEditText.getText().toString();
            text = text.substring(0, startPos - 1) + text.substring(startPos);
            mFocusedEditText.setText(text);
            mFocusedEditText.setSelection(startPos - 1);
        } else {
            int startPos = mFocusedEditText.getSelectionStart();
            String text = mFocusedEditText.getText().toString();
            text = text.substring(0, startPos) + text.substring(mFocusedEditText.getSelectionEnd());
            mFocusedEditText.setText(text);
            mFocusedEditText.setSelection(startPos);
        }
    }

    private View createEmptyEditView(Context context) {
        EditText editText = new EditText(context);
        editText.setMinEms(1);
        editText.setText("0");
        editText.setGravity(Gravity.CENTER);
        editText.setShowSoftInputOnFocus(false);
        editText.setSelectAllOnFocus(true);
        editText.setInputType(
                InputType.TYPE_CLASS_NUMBER |
                        InputType.TYPE_NUMBER_FLAG_SIGNED |
                        InputType.TYPE_NUMBER_FLAG_DECIMAL |
                        InputType.TYPE_CLASS_DATETIME
        );
        editText.setOnFocusChangeListener(this);
        Drawable drawable;
        if (ActivityEssential.isThemeDark(getActivity()))
            drawable = context.getDrawable(R.drawable.cell_shape_light);
        else drawable = context.getDrawable(R.drawable.cell_shape_dark);
        editText.setBackground(drawable);
        GridLayout.LayoutParams lp = new GridLayout.LayoutParams();
        lp.setGravity(Gravity.FILL);
        editText.setLayoutParams(lp);
        return editText;
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!(v instanceof EditText)) return;
        if (hasFocus) {
            mFocusedEditText = (EditText) v;
            mFocusedEditText.selectAll();
        } else if (((EditText) v).getText().toString().isEmpty()) {
            ((EditText) v).setText("0");
        }
    }

    public void addPoint() {
        if (mFocusedEditText == null) return;
        String text = mFocusedEditText.getText().toString();
        text = text.substring(0, mFocusedEditText.getSelectionStart()) + ".";
        int newPosition = text.length();
        text += mFocusedEditText.getText().toString().substring(mFocusedEditText.getSelectionEnd());
        mFocusedEditText.setText(text);
        mFocusedEditText.setSelection(newPosition);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        String[] data = new String[mGridLayout.getChildCount()];
        for (int i = 0; i < mGridLayout.getChildCount(); i++) {
            EditText cell = ((EditText) mGridLayout.getChildAt(i));
            data[i] = cell.getText().toString();
            if (cell.isFocused()) {
                outState.putIntArray("selection", new int[]{i, cell.getSelectionStart(), cell.getSelectionEnd()});
            }
        }
        outState.putStringArray("data", data);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState == null) return;
        String[] data = savedInstanceState.getStringArray("data");
        if (data != null) {
            for (int i = 0; i < mGridLayout.getChildCount(); i++) {
                ((EditText) mGridLayout.getChildAt(i)).setText(data[i]);
            }
        }
        int[] selection = savedInstanceState.getIntArray("selection");
        if (selection != null) {
            EditText cell = ((EditText) mGridLayout.getChildAt(selection[0]));
            cell.requestFocus();
            cell.setSelection(selection[1], selection[2]);
        }
    }

    public void clear() {
        for (int i = 0; i < mGridLayout.getChildCount(); i++) {
            ((EditText) mGridLayout.getChildAt(i)).setText("0");
        }
    }

    public void inputDivide() {
        if (mFocusedEditText == null) return;
        String text = mFocusedEditText.getText().toString();
        text = text.substring(0, mFocusedEditText.getSelectionStart()) + "/";
        int newPosition = text.length();
        text += mFocusedEditText.getText().toString().substring(mFocusedEditText.getSelectionEnd());
        mFocusedEditText.setText(text);
        mFocusedEditText.setSelection(newPosition);
    }
}