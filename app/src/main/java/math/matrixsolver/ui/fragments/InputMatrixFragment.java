package math.matrixsolver.ui.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.FragmentManager;

import com.github.kiprobinson.bigfraction.BigFraction;

import java.util.Objects;

import math.matrixsolver.R;
import math.matrixsolver.matrix.MatrixGenerator;
import math.matrixsolver.ui.activities.TransactionManager;

public class InputMatrixFragment extends CorrectThemedFragment implements KeyboardFragment.KeyboardEvent {
    private MatrixFragment mMatrixFragment = null;
    private KeyboardFragment mKeyboardFragment = null;

    private int mMatrixHSize = 2;
    final static private int MIN_MATRIX_hSIZE = 2;
    public final static int MAX_MATRIX_hSIZE = 15;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_input_matrix, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        FragmentManager fragmentManager = getFragmentManager();
        if (mKeyboardFragment != null) {
            mKeyboardFragment.addKeyboardListener(this);
            return;
        }
        if (fragmentManager == null || savedInstanceState != null) return;
        mMatrixFragment = new MatrixFragment();
        mKeyboardFragment = new KeyboardFragment();
        mKeyboardFragment.addKeyboardListener(this);
        fragmentManager.beginTransaction()
                .add(R.id.matrix_layout, mMatrixFragment)
                .add(R.id.keyboard_layout, mKeyboardFragment)
                .commit();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onKeyNumber(int number) {
        if (mMatrixFragment != null) mMatrixFragment.inputNumber(number);
    }

    @Override
    public void onKeyAction(int actionType) {
        if (mMatrixFragment == null) return;
        switch (actionType) {
            case ACTION_SIGN:
                mMatrixFragment.changeSign();
                break;
            case ACTION_POINT:
                mMatrixFragment.addPoint();
                break;
            case ACTION_REDUCE:
                if (mMatrixHSize > MIN_MATRIX_hSIZE) {
                    mMatrixFragment.setMatrixSize(--mMatrixHSize);
                    mKeyboardFragment.setMatrixSizeAndColor(mMatrixHSize);
                }
                break;
            case ACTION_INCREASE:
                if (mMatrixHSize < MAX_MATRIX_hSIZE) {
                    mMatrixFragment.setMatrixSize(++mMatrixHSize);
                    mKeyboardFragment.setMatrixSizeAndColor(mMatrixHSize);
                }
                break;
            case ACTION_NEXT:
                mMatrixFragment.nextCell();
                break;
            case ACTION_CLEAR:
                mMatrixFragment.backspace();
                break;
            case ACTION_CLEAR_ALL:
                mMatrixHSize = 2;
                mMatrixFragment.setMatrixSize(mMatrixHSize);
                mKeyboardFragment.setMatrixSizeAndColor(mMatrixHSize);
                mMatrixFragment.clear();
                mMatrixFragment.setFlag(Gravity.CENTER);
                break;
            case DIVIDE:
                mMatrixFragment.inputDivide();
                break;
            case ACTION_SETTING:
                showSettings();
                break;
            case ACTION_HELP:
                showHelp();
                break;
            case ACTION_FILE:
                showFileManager();
                break;
            case ACTION_RESULT:
                solveMatrix();
                break;

        }
    }

    private void showFileManager() {
        Activity activity = getActivity();
        if (mMatrixFragment == null || !(activity instanceof TransactionManager)) return;
        final BigFraction[][] matrix = mMatrixFragment.getMatrix();
        if (matrix != null)
            ((TransactionManager) activity).showFileManager(matrix);
    }

    private void showSettings() {
        Activity activity = getActivity();
        if (mMatrixFragment == null || !(activity instanceof TransactionManager)) return;
        ((TransactionManager) activity).showSettings();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (mKeyboardFragment != null) mKeyboardFragment.removeKeyboardListener();
    }

    private void solveMatrix() {
        Activity activity = getActivity();
        final BigFraction[][] matrix = mMatrixFragment.getMatrix();
        if (mMatrixFragment == null || !(activity instanceof TransactionManager) || matrix == null)
            return;
        ((TransactionManager) activity).showAnswerMatrix(mMatrixFragment.getMatrix());
    }

    private void showHelp() {
        Activity activity = getActivity();
        if (mMatrixFragment == null || !(activity instanceof TransactionManager)) return;
        ((TransactionManager) activity).showHelp();
    }

    public void setMatrix(BigFraction[][] matrix) {
        if (mMatrixFragment == null) return;
        mMatrixFragment.setMatrix(matrix);
        mMatrixHSize = matrix[0].length;
        mKeyboardFragment.setMatrixSizeAndColor(mMatrixHSize);
    }

    public void clear() {
        mMatrixFragment.clear();
    }

    public void generateRandomMatrix(double rootsMin, double rootsMax,
                                     double min, double max, boolean generateFractionalNumbers) {
        new Thread(() -> {
            BigFraction[][] matrix = MatrixGenerator.generateNewMatrix(
                    MatrixGenerator.generateRoots(mMatrixFragment.getMatrixSize() - 1, rootsMin, rootsMax, generateFractionalNumbers),
                    min, max, generateFractionalNumbers);
            Objects.requireNonNull(getActivity()).runOnUiThread(() -> setMatrix(matrix));
        }).start();
    }

    public MatrixFragment getMatrixFragment() {
        return mMatrixFragment;
    }
}
