package math.matrixsolver.ui.fragments;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import math.matrixsolver.ui.activities.ActivityEssential;

public class CorrectThemedFragment extends Fragment {
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ActivityEssential.setupDarkThemeForViewGroupIfNeeded(getActivity(), (ViewGroup) view);
    }
}