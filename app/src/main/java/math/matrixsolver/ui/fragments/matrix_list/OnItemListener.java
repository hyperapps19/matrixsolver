package math.matrixsolver.ui.fragments.matrix_list;

import math.matrixsolver.data.ItemMatrixData;

interface OnItemListener {
    void onClick(ItemMatrixData item);
}
