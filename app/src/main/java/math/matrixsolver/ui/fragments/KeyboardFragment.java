package math.matrixsolver.ui.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import org.jetbrains.annotations.NotNull;

import java.util.Locale;
import java.util.Objects;

import math.matrixsolver.R;

import static math.matrixsolver.ui.fragments.InputMatrixFragment.MAX_MATRIX_hSIZE;

public class KeyboardFragment extends CorrectThemedFragment implements View.OnClickListener {

    private KeyboardEvent mKeyboardListener = null;
    private TextView mMatrixSize = null;

    private final static String MATRIX_SIZE_TEXT_KEY = "matrixSizeText";

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        addOnClickListener(view, new int[]{
                R.id.one, R.id.two, R.id.three, R.id.four, R.id.five,
                R.id.six, R.id.seven, R.id.eight, R.id.nine, R.id.zero,
                R.id.point, R.id.sign, R.id.clear, R.id.next, R.id.file, R.id.result,
                R.id.reduce, R.id.increase, R.id.help, R.id.setting, R.id.collapse_clear, R.id.divide
        });
        mMatrixSize = view.findViewById(R.id.matrix_size);
        setMatrixSizeAndColor(2);
    }


    public void addKeyboardListener(KeyboardEvent event) {
        mKeyboardListener = event;

    }

    public void removeKeyboardListener() {
        mKeyboardListener = null;
    }

    private void addOnClickListener(@NonNull View v, @NonNull @IdRes int[] ids) {
        for (int id : ids) {
            v.findViewById(id).setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View v) {
        if (mKeyboardListener == null) return;
        switch (v.getId()) {
            case R.id.one:
            case R.id.two:
            case R.id.three:
            case R.id.four:
            case R.id.five:
            case R.id.six:
            case R.id.seven:
            case R.id.eight:
            case R.id.nine:
            case R.id.zero:
                mKeyboardListener.onKeyNumber(Integer.parseInt(((Button) v).getText().toString()));
                break;
            default:
                mKeyboardListener.onKeyAction(v.getId());
                break;
        }
    }

    public static String formatMatrixSize(int matrixHSize) {
        return String.format(
                Locale.getDefault(),
                "%dx%d",
                matrixHSize, matrixHSize - 1
        );
    }

    public void setMatrixSizeAndColor(int matrixHSize) {
        if (mMatrixSize != null) {
            mMatrixSize.setText(formatMatrixSize(matrixHSize));
            if (matrixHSize >= (MAX_MATRIX_hSIZE))
                mMatrixSize.setBackgroundColor(Color.parseColor("#c62828"));
            else if (matrixHSize >= (MAX_MATRIX_hSIZE - MAX_MATRIX_hSIZE * 0.3)) // Warning
                mMatrixSize.setBackgroundColor(Color.parseColor("#ef6c00"));
            else
                mMatrixSize.setBackgroundColor(ContextCompat.getColor(Objects.requireNonNull(getContext()), R.color.colorPrimary));
        }
    }

    public interface KeyboardEvent {
        int ACTION_CLEAR = R.id.clear;
        int ACTION_CLEAR_ALL = R.id.collapse_clear;
        int DIVIDE = R.id.divide;
        int ACTION_POINT = R.id.point;
        int ACTION_SIGN = R.id.sign;
        int ACTION_FILE = R.id.file;
        int ACTION_NEXT = R.id.next;
        int ACTION_RESULT = R.id.result;
        int ACTION_REDUCE = R.id.reduce;
        int ACTION_INCREASE = R.id.increase;
        int ACTION_HELP = R.id.help;
        int ACTION_SETTING = R.id.setting;

        void onKeyNumber(int number);

        void onKeyAction(int actionType);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        return inflater.inflate(R.layout.fragment_keyboard, container, false);
    }

    @Override
    public void onSaveInstanceState(@NotNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(MATRIX_SIZE_TEXT_KEY, mMatrixSize.getText().toString());
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if (savedInstanceState != null)
            mMatrixSize.setText(savedInstanceState.getString(MATRIX_SIZE_TEXT_KEY));
    }
}
