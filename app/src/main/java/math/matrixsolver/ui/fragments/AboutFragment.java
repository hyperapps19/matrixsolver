package math.matrixsolver.ui.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Locale;

import math.matrixsolver.BuildConfig;
import math.matrixsolver.R;
import math.matrixsolver.ui.activities.SLAEBasic;


public class AboutFragment extends CorrectThemedFragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView version = view.findViewById(R.id.versionTv);
        version.setText(String.format(Locale.getDefault(), "%s %s %s %d",
                getString(R.string.version),
                BuildConfig.VERSION_NAME,
                getString(R.string.build),
                BuildConfig.VERSION_CODE
        ));
        view.findViewById(R.id.goToProblems).setOnClickListener(v -> startActivity(new Intent(getContext(), SLAEBasic.class)));
    }

}