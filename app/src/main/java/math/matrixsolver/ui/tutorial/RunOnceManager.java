package math.matrixsolver.ui.tutorial;

import android.content.Context;

import androidx.preference.PreferenceManager;

public class RunOnceManager {
    private final String mSettingsValueName;
    private final Context context;

    public RunOnceManager(Context context, String settingsValueName) {
        mSettingsValueName = settingsValueName;
        this.context = context;
    }

    public boolean checkIfNeedToRun() {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean(mSettingsValueName, true);
    }

    public void setIfNeedToRun(boolean isShowcaseShowNeeded) {
        PreferenceManager.getDefaultSharedPreferences(context).edit().putBoolean(mSettingsValueName, isShowcaseShowNeeded).apply();
    }
}