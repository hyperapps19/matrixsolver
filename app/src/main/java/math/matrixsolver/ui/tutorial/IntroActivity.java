package math.matrixsolver.ui.tutorial;

import android.Manifest;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.fragment.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;
import com.github.paolorotolo.appintro.model.SliderPage;

import math.matrixsolver.PermissionManager;
import math.matrixsolver.R;
import math.matrixsolver.ui.activities.RootActivity;

public class IntroActivity extends AppIntro {
    public static final String SETTINGS_KEY_INTRO = "intro";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Resources r = getResources();

        SliderPage[] sliderPages = new SliderPage[5];
        setFadeAnimation();
        final ActionBar bar = getSupportActionBar();
        if (bar != null) bar.hide();

        for (int i = 0, sliderPagesLength = sliderPages.length; i < sliderPagesLength; i++)
            sliderPages[i] = new SliderPage();

        sliderPages[0].setTitle(r.getString(R.string.welcome));
        sliderPages[0].setDescription(r.getString(R.string.welcome_description));
        sliderPages[0].setImageDrawable(R.drawable.ic_first_intro);
        sliderPages[0].setBgColor(Color.parseColor("#424242"));

        sliderPages[1].setTitle(r.getString(R.string.permissions_camera));
        sliderPages[1].setDescription(r.getString(R.string.permissions_camera_description));
        sliderPages[1].setImageDrawable(R.drawable.ic_cam);
        sliderPages[1].setBgColor(Color.parseColor("#00bcd4"));
        requestPermissionsInIntro(new String[]{Manifest.permission.CAMERA}, 2);

        sliderPages[2].setTitle(r.getString(R.string.permissions_external_storage));
        sliderPages[2].setDescription(r.getString(R.string.permissions_external_storage_description));
        sliderPages[2].setImageDrawable(R.drawable.ic_storage);
        sliderPages[2].setBgColor(Color.parseColor("#aa00ff"));
        requestPermissionsInIntro(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE}, 3);
        requestPermissionsInIntro(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 3);

        sliderPages[3].setTitle(r.getString(R.string.permissions_internet));
        sliderPages[3].setDescription(r.getString(R.string.permissions_internet_description));
        sliderPages[3].setImageDrawable(R.drawable.ic_internet_intro);
        sliderPages[3].setBgColor(Color.parseColor("#ff6d00"));
        requestPermissionsInIntro(new String[]{Manifest.permission.INTERNET}, 4);

        sliderPages[4].setTitle(r.getString(R.string.all_set));
        sliderPages[4].setDescription(r.getString(R.string.all_set_description));
        sliderPages[4].setImageDrawable(R.drawable.ic_all_set);
        sliderPages[4].setBgColor(Color.parseColor("#4caf50"));

        for (SliderPage sliderPage : sliderPages)
            addSlide(AppIntroFragment.newInstance(sliderPage));
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);
        passIntro();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);
        passIntro();
    }

    void passIntro() {
        RunOnceManager manager = new RunOnceManager(getBaseContext(), SETTINGS_KEY_INTRO);
        manager.setIfNeedToRun(false);
        startActivity(new Intent(this, RootActivity.class));
        finish();
    }

    void requestPermissionsInIntro(@NonNull String[] permissions, int slideNumber) {
        for (String permission : permissions)
            if (PermissionManager.checkIfNeedToRequestPermission(this, permission) == PermissionManager.NEED_TO_ASK_FOR_PERMISSION)
                askForPermissions(new String[]{permission}, slideNumber);
    }
}