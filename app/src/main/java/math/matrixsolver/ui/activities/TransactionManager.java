package math.matrixsolver.ui.activities;

import com.github.kiprobinson.bigfraction.BigFraction;

public interface TransactionManager {
    void showAnswerMatrix(BigFraction[][] matrix);

    void goBack();

    void showHelp();

    void showSettings();

    void loadMatrix(BigFraction[][] loadMatrix);

    void showFileManager(BigFraction[][] matrix);

    void showQrScanning();

    void startLoadingFromFile();

    void startSavingToFile();
}
