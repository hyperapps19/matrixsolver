package math.matrixsolver.ui.activities;

import android.os.Bundle;
import android.view.View;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import katex.hourglass.in.mathlib.MathView;
import math.matrixsolver.R;
import math.matrixsolver.ui.fragments.ProblemFragment;

public class SLAEBasic extends ActivityEssential implements View.OnClickListener, ProblemFragment.ProblemCallback {
    private int[] buttons = {R.id.btnBack, R.id.btnNext, R.id.btnSolve};
    private int counter;
    private ProblemFragment problemFragment;
    private MathView mMathView;

    private void addFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.problem_frame_layout, fragment);
        transaction.commit();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        ActivityEssential.setTheme(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slae_basic);
        setTitle(R.string.slae_basics);
        for (int button : buttons)
            findViewById(button).setOnClickListener(this);
        counter = 0;
        problemFragment = new ProblemFragment();
        problemFragment.setXmlNumber(0);
        checkButton();
        addFragment(problemFragment);
        mMathView = findViewById(R.id.problemSolvingView);
        ActivityEssential.setupMathView(this, mMathView);
    }

    private void checkButton() {
        findViewById(R.id.btnSolve).setVisibility(View.VISIBLE);
        findViewById(R.id.btnNext).setVisibility(View.VISIBLE);
        if (counter == 0) findViewById(R.id.btnSolve).setVisibility(View.INVISIBLE);
        else if (counter == problemFragment.getXmlsArrLength() - 1)
            findViewById(R.id.btnNext).setVisibility(View.INVISIBLE);
    }

    @Override
    public void onClick(View v) {
        boolean needsUpdate = false;
        switch (v.getId()) {
            case R.id.btnBack:
                if (counter > 0) counter--;
                else finish();
                needsUpdate = true;
                break;
            case R.id.btnNext:
                needsUpdate = true;
                if (counter < problemFragment.getXmlsArrLength() - 1) counter++;
                break;
            case R.id.btnSolve:
                problemFragment.solveProblem();
                break;
        }
        if (needsUpdate) {
            if (mMathView != null) mMathView.setDisplayText(""); // Clear MathView
            problemFragment = new ProblemFragment();
            problemFragment.setXmlNumber(counter);
            problemFragment.registerCallBack(this);
            addFragment(problemFragment);
            checkButton();
        }
    }

    @Override
    public void onProblemSolved(String solving) {
        if (mMathView != null) mMathView.setDisplayText(solving);
    }
}