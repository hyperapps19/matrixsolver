package math.matrixsolver.ui.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.preference.PreferenceManager;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import katex.hourglass.in.mathlib.MathView;
import math.matrixsolver.R;


@SuppressLint("Registered")
public class ActivityEssential extends AppCompatActivity {

    private static String mTheme = null;
    public static String getTheme(Activity activity) {
        if (mTheme == null)
            mTheme = PreferenceManager.getDefaultSharedPreferences(activity).getString("theme", "light");
        return mTheme;
    }

    public static void setTheme(Activity activity) throws Resources.NotFoundException {
        String theme = getTheme(activity);
            assert theme != null;

            switch (theme) {
                case "light":
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    activity.setTheme(R.style.AppTheme);
                    break;
                case "dark":
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    activity.setTheme(R.style.DarkTheme);
                    break;
                default:
                    throw new Resources.NotFoundException();
            }
    }

    public static boolean isThemeDark(Activity activity) {
        return getTheme(activity).equals("dark");
    }

    public static int getThemeOppositeColor(Activity activity) {
        if (isThemeDark(activity)) return Color.WHITE;
        return Color.BLACK;
    }

    public static void applyDarkThemeToImageButtons(ArrayList<View> imageButtons) {
        for (int i = 0; i < imageButtons.size(); i++) {
            final ImageButton imageButton = (ImageButton) imageButtons.get(i);
            if (imageButton != null)
                imageButton.setColorFilter(Color.WHITE);
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        String localeId = preferences.getString("lang", "default");
        if (!(Objects.requireNonNull(localeId).equals("default"))) {
            Locale locale = new Locale(localeId);
            Locale.setDefault(locale);
            Configuration configuration = new Configuration();
            configuration.locale = locale;
            getResources().updateConfiguration(configuration, null);
        }
    }

    public static void addOnClickListener(@NonNull ArrayList<View> touchables, @NonNull View.OnClickListener listener) {
        for (int i = 0; i < touchables.size(); i++) touchables.get(i).setOnClickListener(listener);
    }

    public static ArrayList<View> findAllViews(@NonNull ViewGroup viewGroup, @NonNull Class type) {
        ArrayList<View> array = new ArrayList<>();
        for (int i = 0; i < viewGroup.getChildCount(); i++) {
            final View child = viewGroup.getChildAt(i);
            if (child == null) continue;
            if (type.isInstance(child)) array.add(child);
            else if (child instanceof ViewGroup) {
                array.addAll(findAllViews((ViewGroup) child, type));
            }
        }
        return array;
    }

    public static void setupDarkThemeForViewGroupIfNeeded(Activity activity, ViewGroup viewGroup) {
        if (isThemeDark(activity)) {
            final ArrayList<View> imgBtns = ActivityEssential.findAllViews(viewGroup, ImageButton.class);
            ActivityEssential.applyDarkThemeToImageButtons(imgBtns);
        }
    }

    public static int getThemeColor(Activity activity) {
        TypedValue a = new TypedValue();
        activity.getTheme().resolveAttribute(android.R.attr.windowBackground, a, true);
        return a.data;
    }

    public static void setupMathView(Activity activity, MathView v) {
        v.setViewBackgroundColor(getThemeColor(activity));
        v.setTextColor(getThemeOppositeColor(activity));
    }

}