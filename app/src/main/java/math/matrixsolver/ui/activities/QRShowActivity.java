package math.matrixsolver.ui.activities;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.print.PrintHelper;

import com.rustamg.filedialogs.FileDialog;

import net.glxn.qrgen.android.QRCode;
import net.glxn.qrgen.core.exception.QRGenerationException;
import net.glxn.qrgen.core.image.ImageType;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;

import math.matrixsolver.FileDialogHelper;
import math.matrixsolver.R;

public class QRShowActivity extends ActivityEssential implements FileDialog.OnFileSelectedListener {
    private ImageView QRView;
    private ImageButton QRSaveBtn;
    private ImageButton QRPrintBtn;
    private TextView QRErrorView, mLinkTextView;
    private String qrData;

    private void printPhoto(Bitmap bitmap) {
        PrintHelper photoPrinter = new PrintHelper(this);
        photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FIT);
        photoPrinter.printBitmap("MatrixSolver QR Print", bitmap);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        ActivityEssential.setTheme(this);
        setTitle(R.string.qr_show_title);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_qr_show);
        qrData = getIntent().getStringExtra("qrData");
        QRView = findViewById(R.id.qrView);
        QRErrorView = findViewById(R.id.qrErrorView);
        QRSaveBtn = findViewById(R.id.QRsaveBtn);
        QRPrintBtn = findViewById(R.id.QRprintBtn);
        mLinkTextView = findViewById(R.id.linkField);
        QRPrintBtn.setOnClickListener(v -> printPhoto(createQRCodeBitmap(qrData, "light", getQRSize())));
        QRSaveBtn.setOnClickListener(v -> FileDialogHelper.initFileDialog(QRShowActivity.this, FileDialogHelper.TYPE_SAVE_FILE));
        setView();
    }

    @SuppressLint("SetTextI18n")
    private void setView() {
        try {
            QRView.setImageBitmap(createQRCodeBitmap(qrData, ActivityEssential.getTheme(this), getQRSize()));
            QRSaveBtn.setEnabled(true);
            QRPrintBtn.setEnabled(true);
        } catch (QRGenerationException e) { // Data is too big
            QRErrorView.setText(R.string.qr_error);
            QRSaveBtn.setEnabled(false);
            QRPrintBtn.setEnabled(false);
        }

        if (getIntent().getBooleanExtra("isLink", false))
            mLinkTextView.setText("URL: https://pastebin.com/" + qrData);
    }

    private int getQRSize() {
        DisplayMetrics displayMetrics = getResources().getDisplayMetrics();
        return displayMetrics.heightPixels > displayMetrics.widthPixels ? displayMetrics.heightPixels : displayMetrics.widthPixels;
    }

    private Bitmap createQRCodeBitmap(String data, String theme, int qrSize) {

        QRCode code = QRCode.from(data);
        if (theme.equals("dark")) { //black background, white QR
            code.withColor(Color.WHITE, Color.TRANSPARENT);
            QRSaveBtn.setColorFilter(Color.WHITE);
            QRPrintBtn.setColorFilter(Color.WHITE);
        } else //white background, black QR
            code.withColor(Color.BLACK, Color.TRANSPARENT);
        return Bitmap.createScaledBitmap(code.bitmap(), qrSize, qrSize, false);
    }

    private void saveQRCodePNG(OutputStream outs) {
        QRCode code = QRCode.from(getIntent().getStringExtra("data"));
        code.withColor(Color.BLACK, Color.WHITE);
        code.to(ImageType.PNG).writeTo(outs);
    }

    @Override
    public void onFileSelected(FileDialog dialog, File file) {
        try {
            if (FileDialogHelper.checkIfFileCanBeSaved(this)) {
                if (!file.exists())
                    file.createNewFile();
                OutputStream outs = new FileOutputStream(FileDialogHelper.addExtensionIfNeeded(file.getName(), ".matrix"));
                saveQRCodePNG(outs);
            }
        } catch (Exception e) {
            FileDialogHelper.showOperatingWithFilesError(this);
        }
    }
}