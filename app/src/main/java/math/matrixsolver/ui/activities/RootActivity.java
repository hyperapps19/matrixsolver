package math.matrixsolver.ui.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.preference.PreferenceManager;

import com.github.kiprobinson.bigfraction.BigFraction;
import com.rustamg.filedialogs.FileDialog;
import com.rustamg.filedialogs.OpenFileDialog;

import java.io.File;
import java.util.HashSet;
import java.util.Locale;
import java.util.Objects;
import java.util.Set;

import math.matrixsolver.FileDialogHelper;
import math.matrixsolver.R;
import math.matrixsolver.matrix.Matrix;
import math.matrixsolver.matrix.solver.IterationSolver;
import math.matrixsolver.matrix.solver.SolveThread;
import math.matrixsolver.matrix.solver.Solver;
import math.matrixsolver.matrix.solver.methods.GaussSeidelSolver;
import math.matrixsolver.matrix.solver.methods.GaussSolver;
import math.matrixsolver.matrix.solver.methods.JacobiSolver;
import math.matrixsolver.matrix.solver.methods.MultithreadedJacobiSolver;
import math.matrixsolver.ui.fragments.AdditionalButtons;
import math.matrixsolver.ui.fragments.InputMatrixFragment;
import math.matrixsolver.ui.fragments.MatrixFragment;
import math.matrixsolver.ui.fragments.ToolbarFragment;
import math.matrixsolver.ui.tutorial.IntroActivity;
import math.matrixsolver.ui.tutorial.RunOnceManager;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;

import static math.matrixsolver.FileDialogHelper.TYPE_OPEN_FILE;
import static math.matrixsolver.FileDialogHelper.TYPE_SAVE_FILE;
import static math.matrixsolver.ui.fragments.matrix_list.MatrixListFragment.getCompressedMatrix;
import static math.matrixsolver.ui.fragments.matrix_list.MatrixListFragment.matrixFromBytes;

public class RootActivity extends ActivityEssential implements TransactionManager, SolveThread.OnSolveCallback, AdditionalButtons.NormalAdditionalButtonsEvent, FileDialog.OnFileSelectedListener {
    public static final String SETTINGS_KEY_SHOWCASE = "showcase";
    AdditionalButtons mAdditionalButtonsFragment;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        ActivityEssential.setTheme(this);
        checkAndRunIntroIfNeeded();
        super.onCreate(savedInstanceState);
        ViewGroup container = new FrameLayout(this);
        container.setLayoutParams(new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        ));
        container.setId(R.id.root_container);
        setContentView(container);
        if (savedInstanceState == null) {
            InputMatrixFragment fragment = new InputMatrixFragment();
            getSupportFragmentManager().beginTransaction().add(
                    R.id.root_container,
                    fragment,
                    InputMatrixFragment.class.getName()
            ).setPrimaryNavigationFragment(fragment).commit();
        }
        mAdditionalButtonsFragment = setupAdditionalButtons(new AdditionalButtons());
    }

    private AdditionalButtons setupAdditionalButtons(AdditionalButtons fragment) {
        fragment.addToolbarListener(this);
        getSupportFragmentManager().beginTransaction().replace(R.id.additional_buttons, fragment).commit();
        return fragment;
    }

    void checkAndRunIntroIfNeeded() {
        RunOnceManager mgr = new RunOnceManager(getBaseContext(), "intro");
        if (mgr.checkIfNeedToRun()) {
            startActivity(new Intent(this, IntroActivity.class));
            finish();
        }
    }

    void displayShowcases(View[] targets, int[] titleTexts, int[] contentTexts, int dismissText, int skipText, RunOnceManager manager) {
        Set<Integer> lengths = new HashSet<>();
        lengths.add(targets.length);
        lengths.add(titleTexts.length);
        lengths.add(contentTexts.length);
        if (lengths.size() != 1) return;
        int length = ((int) Objects.requireNonNull(lengths.toArray())[0]);
        lengths.clear();
        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(this);
        for (int i = 0; i < length; i++) {
            sequence.addSequenceItem(new MaterialShowcaseView.Builder(this)
                    .setTarget(targets[i])
                    .setTitleText(titleTexts[i])
                    .setContentText(contentTexts[i])
                    //.setSkipText(skipText)
                    .setDismissText(dismissText).build());
        }
        sequence.start();

        final int[] counter = {0};
        sequence.setOnItemShownListener((itemView, position) -> {
            counter[0]++;
            if (counter[0] >= length - 1) manager.setIfNeedToRun(false);
        });

    }


    @Override
    protected void onPostCreate(@Nullable Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        RunOnceManager manager = new RunOnceManager(getBaseContext(), SETTINGS_KEY_SHOWCASE);
        if (new RunOnceManager(getBaseContext(), SETTINGS_KEY_SHOWCASE).checkIfNeedToRun()) {
            View[] targets = {new View(this), new View(this), findViewById(R.id.matrix_size),
                    findViewById(R.id.randomGenerationButton), findViewById(R.id.eraseMatrixButton),
                    findViewById(R.id.reduce), findViewById(R.id.increase), findViewById(R.id.methods_spinner), findViewById(R.id.collapse_clear),
                    findViewById(R.id.clear), findViewById(R.id.next), findViewById(R.id.file), findViewById(R.id.result)};

            int[] titleTexts = {R.string.showcase_title_0, R.string.showcase_title_1, R.string.showcase_title_2,
                    R.string.showcase_title_3, R.string.showcase_title_4, R.string.showcase_title_5, R.string.showcase_title_6,
                    R.string.showcase_title_7, R.string.showcase_title_7_2, R.string.showcase_title_8, R.string.showcase_title_9, R.string.showcase_title_10,
                    R.string.showcase_title_11};

            int[] contentTexts = {R.string.showcase_0, R.string.showcase_1, R.string.showcase_2,
                    R.string.showcase_3, R.string.showcase_4, R.string.showcase_5, R.string.showcase_6,
                    R.string.showcase_7, R.string.showcase_7_2, R.string.showcase_8, R.string.showcase_9, R.string.showcase_10,
                    R.string.showcase_11};

            int dismissText = R.string.showcase_dismiss;
            int skipText = R.string.showcase_skip;

            displayShowcases(targets, titleTexts, contentTexts, dismissText, skipText, manager);

        }
    }


    private IterationSolver setupIterationSolver(IterationSolver solver) {
        final SharedPreferences p = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        solver.setIterationPrecision(BigFraction.valueOf(Objects.requireNonNull(p.getString("iteration_precision", "0.0001"))));
        solver.setMaxIterations(Integer.parseInt(Objects.requireNonNull(p.getString("iterations", "100"))));
        solver.setSourceRoot(BigFraction.valueOf(Objects.requireNonNull(p.getString("base_root", "1"))));
        return solver;
    }


    @Override
    public void showAnswerMatrix(BigFraction[][] matrix) {
        //TODO: добавить метод, который запускает асинхронное решение матрицы с колбеком
        //TODO: add progress bar
        Solver solver = null;
        switch (((Spinner) findViewById(R.id.methods_spinner)).getSelectedItemPosition()) {
            case 0: // Gauss method
                solver = new GaussSolver(this);
                break;
            case 1: // Gauss-Seidel method
                solver = setupIterationSolver(new GaussSeidelSolver(this));
                break;
        }

        SolveThread mSolvingThread = new SolveThread(this, new Matrix(matrix), solver, getResources());
        mSolvingThread.registerCallBack(this);
        mSolvingThread.start();
    }

    private void showAnswerMatrix(String answer, String detailedSolution) {
        Bundle args = new Bundle();
        args.putString("Answer", answer);
        args.putString("DetailedSolution", detailedSolution);
        ToolbarFragment fragment = ToolbarFragment.newInstance(R.string.answer, args);
        addFragment(fragment);
    }

    @Override
    public void goBack() {
        onBackPressed();
    }

    @Override
    public void showHelp() {
        ToolbarFragment fragment = ToolbarFragment.newInstance(R.string.about);
        addFragment(fragment);
    }

    @Override
    public void showSettings() {
        ToolbarFragment fragment = ToolbarFragment.newInstance(R.string.settings);
        addFragment(fragment);
    }

    @Override
    public void loadMatrix(BigFraction[][] loadMatrix) {
        if (loadMatrix != null) {
            goBack();
            ((InputMatrixFragment) Objects.requireNonNull(getSupportFragmentManager().getPrimaryNavigationFragment())).setMatrix(loadMatrix);
        } else Toast.makeText(this, R.string.network_err, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showFileManager(BigFraction[][] matrix) {
        Bundle args = new Bundle();
        args.putInt("Length", matrix.length);
        for (int i = 0; i < matrix.length; i++) {
            String[] array = new String[matrix[i].length];
            for (int j = 0; j < matrix[i].length; j++) {
                array[j] = matrix[i][j].toString();
            }
            args.putStringArray(String.format(Locale.ENGLISH, "Matrix%d", i), array);
        }
        addFragment(ToolbarFragment.newInstance(R.string.filemanager, args));
    }

    @Override
    public void showQrScanning() {
        addFragment(ToolbarFragment.newInstance(R.string.scan_qr));
    }


    private void addFragment(Fragment fragment) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment lastFragment;
        if (fragmentManager.getBackStackEntryCount() > 0) {
            lastFragment = fragmentManager.findFragmentByTag(
                    fragmentManager.getBackStackEntryAt(
                            fragmentManager.getBackStackEntryCount() - 1
                    ).getName()
            );
        } else {
            lastFragment = fragmentManager.getPrimaryNavigationFragment();
        }
        if (lastFragment == null) {
            Log.e("FragmentManagerError", "Last fragment not found!");
            return;
        }
        fragmentManager.beginTransaction()
                .addToBackStack(lastFragment.getClass().getName())
                .hide(lastFragment)
                .add(
                        R.id.root_container,
                        fragment,
                        fragment.getClass().getName()
                ).commit();
    }

    @Override
    public void onBackPressed() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        String lastFragmentTag = null;
        if (fragmentManager.getBackStackEntryCount() > 0) {
            lastFragmentTag = fragmentManager
                    .getBackStackEntryAt(fragmentManager.getBackStackEntryCount() - 1)
                    .getName();
        }
        super.onBackPressed();
        if (lastFragmentTag != null) {
            Fragment fragment = fragmentManager.findFragmentByTag(lastFragmentTag);
            if (fragment == null) {
                Log.e("ERROR", "Unknown error");
                return;
            }
            fragmentManager.beginTransaction().show(fragment).commit();
        }
    }



    double getDoublePreference(String key, String defaultValue) {
        return Double.parseDouble(Objects.requireNonNull(
                PreferenceManager.getDefaultSharedPreferences(getBaseContext()).getString(key, defaultValue)));
    }

    @Override
    public void onKeyAction(int actionType) {
        final InputMatrixFragment inputMatrixFragment = (InputMatrixFragment) Objects.requireNonNull(getSupportFragmentManager().getPrimaryNavigationFragment());
        switch (actionType) {
            case ACTION_GENERATE_MATRIX:
                double genMin = getDoublePreference("generate_from", "1");
                double genMax = getDoublePreference("generate_to", "10");
                boolean genFractional = PreferenceManager.getDefaultSharedPreferences(getBaseContext())
                        .getBoolean("fractional", false);
                inputMatrixFragment.generateRandomMatrix(genMin, genMax, genMin, genMax, genFractional);
                break;
            case ACTION_ERASE_MATRIX:
                inputMatrixFragment.clear();
                break;
        }
    }

    @Override
    public void startLoadingFromFile() {
        FileDialog openFileDialog = FileDialogHelper.initFileDialog(this, TYPE_OPEN_FILE);
        if (openFileDialog != null) {
            Bundle args = new Bundle();
            args.putString(FileDialog.EXTENSION, "matrix");
            openFileDialog.setArguments(args);
        }
    }

    @Override
    public void startSavingToFile() {
        FileDialogHelper.initFileDialog(this, TYPE_SAVE_FILE);
    }

    @Override
    public void onFileSelected(@NonNull FileDialog dialog, @NonNull File file) {
        try {
            final MatrixFragment matrixFragment = ((InputMatrixFragment) Objects.requireNonNull(getSupportFragmentManager().getPrimaryNavigationFragment())).getMatrixFragment();
            if (dialog.getClass().equals(OpenFileDialog.class)) {
                matrixFragment.setMatrix(matrixFromBytes(FileDialogHelper.readFileToBytes(file)));
                goBack();
            } else
                FileDialogHelper.saveFileFromBytes(file, this, getCompressedMatrix(matrixFragment.getMatrix()));
        } catch (Exception e) {
            FileDialogHelper.showOperatingWithFilesError(this);
        }
    }

    @Override
    public void onMatrixSolved(String answerTeX, String solutionTeX) {
        showAnswerMatrix(answerTeX, solutionTeX);
    }
}
