package math.matrixsolver.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DBHelper extends SQLiteOpenHelper {

    public static final String DBName = "Matrices";

    public DBHelper(@Nullable Context context, @Nullable String name, @Nullable SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    private void createStructure(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE \"" + DBName + "\" (\n" +
                "\t\"Name\"\tTEXT NOT NULL,\n" +
                "\t\"Size\"\tTEXT NOT NULL, \n" +
                "\t\"CreationDate\"\tINTEGER NOT NULL, \n" +
                "\t\"Data\"\tBLOB, \n" +
                "\t\"URL\"\tTEXT \n" +
                ")");
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createStructure(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DBName); // Flush
        createStructure(db);
    }
}
