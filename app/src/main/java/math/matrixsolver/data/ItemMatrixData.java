package math.matrixsolver.data;

public class ItemMatrixData {
    private final String mName;
    private final String mSize;
    private final String mDate;
    private final boolean mIsLocal;
    private final int mRowId;

    public ItemMatrixData(int rowId, String name, String size, String date, boolean isLocal) {
        mRowId = rowId;
        mName = name;
        mIsLocal = isLocal;
        mSize = size;
        mDate = date;
    }

    public int getRowId() {
        return mRowId;
    }

    public boolean isLocal() {
        return mIsLocal;
    }

    public String getName() {
        return mName;
    }

    public String getSize() {
        return mSize;
    }

    public String getDate() {
        return mDate;
    }
}
