package math.matrixsolver.matrix;


import com.github.kiprobinson.bigfraction.BigFraction;

public class Matrix implements Cloneable {
    private BigFraction[][] matrix;

    public Matrix(BigFraction[][] matrix) throws ArithmeticException {
        if (!((matrix.length + 1) == (matrix[0].length))) {
            throw new ArithmeticException("Matrix is not n and n+1");
        }
        this.matrix = matrix;
    }

    public static BigFraction[] subtractStrings(BigFraction[] minuend, BigFraction[] subtrahend) throws ArrayIndexOutOfBoundsException {
        if (minuend.length != subtrahend.length) {
            throw new ArrayIndexOutOfBoundsException("Strings length are not same");
        }
        for (int i = 0; i < subtrahend.length; i++) {
            minuend[i] = minuend[i].subtract(subtrahend[i]);
        }
        return minuend;
    }

    public static BigFraction[] addStrings(BigFraction[] augend, BigFraction[] addend) throws ArrayIndexOutOfBoundsException {
        if (augend.length != addend.length) {
            throw new ArrayIndexOutOfBoundsException("Strings length are not same");
        }
        for (int i = 0; i < augend.length; i++) {
            augend[i] = augend[i].add(addend[i]);
        }
        return augend;
    }

    public BigFraction[] multiplyString(int stringNumber, BigFraction multiplier) {
        BigFraction[] string = getString(stringNumber);
        for (int i = 0; i < string.length; i++) string[i] = string[i].multiply(multiplier);
        return string;
    }

    @Override
    public Matrix clone() {
        BigFraction[][] matrixArray2 = new BigFraction[matrix.length][matrix[0].length];
        for (int i = 0; i < matrix.length; i++) matrixArray2[i] = matrix[i].clone();
        return new Matrix(matrixArray2);
    }

    public BigFraction[][] getMatrix() {
        return matrix;
    }

    public BigFraction[] getString(int pos) {
        return matrix[pos];
    }

    public void setString(BigFraction[] string, int pos) throws ArrayIndexOutOfBoundsException {
        if (pos > matrix.length || pos < 0) {
            throw new ArrayIndexOutOfBoundsException("Invalid string number");
        }
        this.matrix[pos] = string;
    }

    public void swapColumns(int pos1, int pos2) {
        BigFraction temp;
        for (int i = 0; i < matrix.length; i++) {
            temp = matrix[i][pos1];
            matrix[i][pos1] = matrix[i][pos2];
            matrix[i][pos2] = temp;
        }
    }
}