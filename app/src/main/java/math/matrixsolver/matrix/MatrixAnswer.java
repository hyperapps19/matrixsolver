package math.matrixsolver.matrix;

public class MatrixAnswer {
    private String solvingLog;
    private String rootsTeX;

    public MatrixAnswer(String solvingLog, String rootsTeX) {
        this.solvingLog = solvingLog;
        this.rootsTeX = rootsTeX;
    }

    public String getSolvingLog() {
        return solvingLog;
    }

    public String getRootsTeX() {
        return rootsTeX;
    }
}
