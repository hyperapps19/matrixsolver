package math.matrixsolver.matrix.solver;

import androidx.annotation.Nullable;

import com.github.kiprobinson.bigfraction.BigFraction;

import math.matrixsolver.matrix.Matrix;

public class SolvingIsRight {
    final boolean solvingIsRight;
    private final BigFraction maxCalculatingError;
    private final BigFraction currentCalculatingError;

    public SolvingIsRight(BigFraction maxCalculatingError, BigFraction currentCalculatingError) {
        this.solvingIsRight = currentCalculatingError.compareTo(maxCalculatingError) <= 0;
        this.maxCalculatingError = maxCalculatingError;
        this.currentCalculatingError = currentCalculatingError;
    }

    public static SolvingIsRight checkIfSolvingIsRight(Matrix matrix, BigFraction[] roots, @Nullable BigFraction maxCalculatingError, @Nullable BigFraction maxCalculatingErrorDivisor) {
        BigFraction error = BigFraction.ZERO, temp = BigFraction.ZERO, temp2 = BigFraction.ZERO;
        for (int y = 0; y < matrix.getMatrix().length; y++) {
            temp2 = temp2.add(matrix.getString(y)[matrix.getString(y).length - 1]);
            for (int x = 0; x < matrix.getMatrix()[0].length - 1; x++) {
                temp = temp.add(matrix.getMatrix()[y][x].multiply(roots[x]));
            }
            error = error.add(matrix.getMatrix()[y][matrix.getMatrix()[0].length - 1].subtract(temp)).abs();
            temp = BigFraction.ZERO;
        }
        if (maxCalculatingError != null)
            return new SolvingIsRight(maxCalculatingError, error);
        else {
            BigFraction maxErr = temp2.divide(maxCalculatingErrorDivisor);
            return new SolvingIsRight(maxErr, error);
        }
    }

    public boolean isSolvingRight() {
        return solvingIsRight;
    }

    public BigFraction getMaxCalculatingError() {
        return maxCalculatingError;
    }

    public BigFraction getCurrentCalculatingError() {
        return currentCalculatingError;
    }
}
