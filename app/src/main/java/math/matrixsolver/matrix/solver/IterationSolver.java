package math.matrixsolver.matrix.solver;

import android.content.Context;
import android.content.res.Resources;

import com.github.kiprobinson.bigfraction.BigFraction;

import java.util.ArrayList;
import java.util.Arrays;

import math.matrixsolver.R;
import math.matrixsolver.matrix.Matrix;
import math.matrixsolver.matrix.TeXConverter;

public abstract class IterationSolver extends Solver {
    private int maxIterations = 150;
    private BigFraction iterationPrecision = new BigFraction("1/1000");
    private BigFraction sourceRoot;

    public IterationSolver(Context context) {
        super(context);
    }


    public void setSourceRoot(BigFraction sourceRoot) {
        this.sourceRoot = sourceRoot;
    }

    public void setMaxIterations(int iterations) {
        this.maxIterations = iterations;
    }

    public void setIterationPrecision(BigFraction iterationPrecision) {
        this.iterationPrecision = iterationPrecision;
    }

    private ArrayList<Integer> getColumnsNotMaxDiagonal(BigFraction[][] matrix) {
        ArrayList<Integer> notMaxDiagonalColumns = new ArrayList<>();
        for (int i = 0; i < matrix[0].length; i++)
            if (!(checkIfElementOnMainDiagonalIsDominating(matrix[i], i)))
                notMaxDiagonalColumns.add(i);
        System.out.println(Arrays.toString(notMaxDiagonalColumns.toArray()));
        return notMaxDiagonalColumns;
    }

    private Matrix optimizeMatrix(Matrix m) {

        BigFraction[][] matrix = m.clone().getMatrix();
        BigFraction[] elements = new BigFraction[matrix.length];
        for (int i = 0; i < matrix.length; i++) elements[i] = matrix[i][i];
        Arrays.sort(elements);
        for (int k = 0; k < elements.length; k++)
            for (int i = 0; i < matrix.length; i++) {
                if (matrix[i][i].equals(elements[k])) {
                    BigFraction[] tempStr;
                    tempStr = matrix[k].clone();
                    matrix[k] = matrix[i];
                    matrix[i] = tempStr;
                }
            }

        for (int k = 0; k < elements.length; k++)
            if (!(checkIfElementOnMainDiagonalIsDominating(matrix[k], k))) {
                // Try to make this element dominating.
                boolean dominating = false;
                BigFraction[] str = matrix[k];
                for (int i = 0; i < matrix.length; i++) {
                    if (i == k) continue;
                    BigFraction[] addStr = matrix[i];
                    final BigFraction[] result = Matrix.addStrings(str, addStr);
                    if (checkIfElementOnMainDiagonalIsDominating(result, k)) {
                        matrix[k] = result;
                        dominating = true;
                        break;
                    }
                }
                if (dominating) continue;
                for (int i = 0; i < matrix.length; i++) {
                    if (i == k) continue;
                    BigFraction[] minusStr = matrix[i];
                    final BigFraction[] result = Matrix.subtractStrings(str, minusStr);
                    if (checkIfElementOnMainDiagonalIsDominating(result, k)) {
                        matrix[k] = result;
                        // dominating = true;
                        break;
                    }
                }
            }


        return new Matrix(matrix);
    }

    private boolean checkIfElementOnMainDiagonalIsDominating(BigFraction[] string, int offset) {
        for (int i = 0; i < string.length - 1; i++) {
            if (i == offset) continue;
            if (string[i].compareTo(string[offset]) >= 0) return false;
        }
        return true;
    }



    public abstract BigFraction[] doIterations(BigFraction[][] matrix, BigFraction[] sourceRoots, BigFraction iterationPrecision, int maxIterations);


    public BigFraction[] solveMatrix(Matrix matrixObject) {
        Resources res = context.getResources();
        Matrix matrixObject1 = matrixObject.clone();
        sb = new StringBuilder();
        sb.append("<h><center>").append(res.getString(R.string.iteration_title))
                .append("</center></h>").append(newline).append(newline);
        sb.append("<h><center>").append(res.getString(R.string.solvingsteps_changing_strings_pos))
                .append("</center></h>").append(newline)
                .append(res.getString(R.string.solvingsteps_src_matrix)).append(": ").append(newline)
                .append(TeXConverter.matrixToTeX(matrixObject1)).append(newline);

        matrixObject1 = optimizeMatrix(matrixObject1);
        BigFraction[][] matrixDominant = matrixObject1.getMatrix();
        BigFraction[] roots1 = new BigFraction[matrixDominant.length];
        Arrays.fill(roots1, sourceRoot);
        sb.append(res.getString(R.string.solvingsteps_converted_matrix)).append(": ").append(newline)
                .append(TeXConverter.matrixToTeX(matrixObject1));
        sb.append("<h><center>").append(res.getString(R.string.solvingsteps_make_roots_closer_to_real_roots))
                .append("</center></h>").append(newline);
        sb.append(res.getString(R.string.solvingsteps_source_roots_is_arr)).append(" ").append(TeXConverter.BigFractionToTeX(roots1[0]))
                .append(" (").append(res.getString(R.string.solvingsteps_this_param_change_settings))
                .append(")").append(newline);
        return doIterations(matrixDominant, roots1, iterationPrecision, maxIterations);
    }

    private BigFraction calcDivergence(BigFraction[] srcRoots, BigFraction[] roots) {
        BigFraction divergence = BigFraction.ZERO;
        for (int i = 0; i < srcRoots.length; i++)
            divergence.add(srcRoots[i].subtract(roots[i]).abs());
        return divergence;

    }
}