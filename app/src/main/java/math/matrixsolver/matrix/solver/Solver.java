package math.matrixsolver.matrix.solver;

import android.content.Context;

import com.github.kiprobinson.bigfraction.BigFraction;

import math.matrixsolver.matrix.Matrix;
import math.matrixsolver.matrix.solver.methods.GaussSolver;

public abstract class Solver {

    protected final String newline = "<br>";
    protected StringBuilder sb = new StringBuilder();

    protected Context context;

    public String getSolvingLog() {
        return sb.toString();
    }

    public abstract BigFraction[] solveMatrix(Matrix matrixObject) throws GaussSolver.InfinityRootsException, GaussSolver.NoRootsException;

    public Solver(Context context) {
        this.context = context;
    }

    public class InfinityRootsException extends Exception {
        public InfinityRootsException() {
            super();
        }
    }

    public class NoRootsException extends Exception {
        public NoRootsException() {
            super();
        }
    }
}
