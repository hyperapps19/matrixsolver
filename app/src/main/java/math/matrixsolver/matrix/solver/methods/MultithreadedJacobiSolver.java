package math.matrixsolver.matrix.solver.methods;

import android.content.Context;
import android.content.res.Resources;

import com.github.kiprobinson.bigfraction.BigFraction;

import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;

import math.matrixsolver.R;
import math.matrixsolver.matrix.TeXConverter;
import math.matrixsolver.matrix.solver.IterationSolver;

public class MultithreadedJacobiSolver extends IterationSolver {
    private CyclicBarrier barrier;
    private volatile BigFraction[] roots;
    private BigFraction[] sourceRoots;

    public BigFraction[] getSourceRoots() {
        return sourceRoots;
    }

    public MultithreadedJacobiSolver(Context context) {
        super(context);
    }

    public synchronized BigFraction[] doIterations(BigFraction[][] matrix, BigFraction[] sourceRoots, BigFraction iterationPrecision, int maxIterations) {
        Resources res = context.getResources();
        this.sourceRoots = sourceRoots;
        roots = sourceRoots.clone();
        final boolean[] iterate = {true};
        int k = 0;
        IterationThread[] iterationThreads = new IterationThread[roots.length];
        barrier = new CyclicBarrier(iterationThreads.length + 1, () -> {
            for (int b = 0; b < matrix.length; b++) {
                iterate[0] = false;
                if (roots[b].subtract(getSourceRoots()[b]).abs().compareTo(iterationPrecision) > 0) {
                    iterate[0] = true;
                    break;
                }
                this.sourceRoots = roots.clone();
            }
        });
        for (int i = 0; i < iterationThreads.length; i++) {
            iterationThreads[i] = new IterationThread(matrix[i], i);
            iterationThreads[i].start();
        }

        while (true) {
            try {
                barrier.await();
            } catch (BrokenBarrierException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            if (!iterate[0] || k > maxIterations) {
                for (IterationThread iterationThread : iterationThreads) {
                    iterationThread.cancel();
                    iterationThread.interrupt();
                    try {
                        iterationThread.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                barrier.reset();
                return roots;
            }

            sb.append(res.getString(R.string.solvingsteps_iteration)).append(" №")
                    .append(k + 1).append(":").append(newline)
                    .append(res.getString(R.string.solvingsteps_roots_closer))
                    .append(TeXConverter.arrayToTex(roots)).append(newline);

            k++;

        }
    }

    class IterationThread extends Thread {

        private BigFraction[] string;
        private int offset;
        private BigFraction root;

        private volatile boolean cancelled;

        IterationThread(BigFraction[] string, int offset) {
            this.string = string;
            this.offset = offset;
        }

        @Override
        public void run() {
            super.run();
            while (!isCancelled()) {
                this.root = new BigFraction(string[string.length - 1]);
                for (int j = 0; j < string.length - 1; j++)
                    if (offset != j) this.root = this.root.subtract(roots[j].multiply(string[j]));
                this.root = root.divide(string[offset]);
                roots[offset] = this.root;
                try {
                    barrier.await();
                } catch (BrokenBarrierException e) {
                    e.printStackTrace();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void cancel() {
            cancelled = true;
        }

        public boolean isCancelled() {
            return cancelled;
        }
    }
}