package math.matrixsolver.matrix.solver;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;

import com.github.kiprobinson.bigfraction.BigFraction;

import java.math.RoundingMode;
import java.util.Objects;

import math.matrixsolver.R;
import math.matrixsolver.matrix.Matrix;
import math.matrixsolver.matrix.TeXConverter;
import math.matrixsolver.matrix.solver.methods.GaussSolver;

public class SolveThread extends Thread {

    private final Matrix matrix;
    private Solver solver;
    private Resources res;
    private OnSolveCallback callback;
    private Context mContext;

    public SolveThread(Context context, Matrix matrix, Solver solver, Resources res) {
        this.matrix = matrix;
        this.solver = solver;
        this.res = res;
        this.mContext = context;
    }

    public void registerCallBack(OnSolveCallback callback) {
        this.callback = callback;
    }

    public void unregisterCallBack() {
        this.callback = null;
    }

    private static RoundingMode getRoundingMode(SharedPreferences preferences, String key) {
        RoundingMode[] roundingModes = {RoundingMode.UP, RoundingMode.DOWN, RoundingMode.CEILING, RoundingMode.FLOOR, RoundingMode.HALF_UP, RoundingMode.HALF_DOWN, RoundingMode.HALF_EVEN, RoundingMode.UNNECESSARY};
        return roundingModes[Integer.parseInt(Objects.requireNonNull(preferences.getString(key, "5")))];
    }

    @Override
    public void run() {
        super.run();
        BigFraction[] roots = null;
        String rootsTeX = null;

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
        int digits_sv = Integer.parseInt(Objects.requireNonNull(prefs.getString("digits_sv", "5")));
        RoundingMode roundingMode_sv = getRoundingMode(prefs, "rounding_mode_sv");
        getRoundingMode(prefs, "rounding_mode_sv");
            try {
                if (!(solver instanceof GaussSolver)) new GaussSolver(mContext).solveMatrix(matrix);
            } catch (GaussSolver.InfinityRootsException e) {
                callback.onMatrixSolved(res.getString(R.string.inf_roots), solver.getSolvingLog());
                return;
            } catch (GaussSolver.NoRootsException e) {
                callback.onMatrixSolved(res.getString(R.string.no_roots), solver.getSolvingLog());
                return;
            } catch (Exception e) {
                if (solver instanceof GaussSolver) {
                    callback.onMatrixSolved(res.getString(R.string.solving_error), solver.getSolvingLog());
                    return;
                }
            }

        try {
            roots = solver.solveMatrix(matrix);
            rootsTeX = TeXConverter.rootsToTeX(roots, digits_sv, roundingMode_sv);
        } catch (GaussSolver.InfinityRootsException e) {
            callback.onMatrixSolved(res.getString(R.string.inf_roots), solver.getSolvingLog());
            return;
        } catch (GaussSolver.NoRootsException e) {
            callback.onMatrixSolved(res.getString(R.string.no_roots), solver.getSolvingLog());
            return;
        } catch (Exception e) {
            if (solver instanceof GaussSolver) {
                callback.onMatrixSolved(res.getString(R.string.solving_error), solver.getSolvingLog());
                return;
            }
        }
        BigFraction maxCalculatingErrorDivisor = new BigFraction(Objects.requireNonNull(PreferenceManager.getDefaultSharedPreferences(mContext).getString("max_calculating_error_divisor", "10")));
        SolvingIsRight s = SolvingIsRight.checkIfSolvingIsRight(matrix, roots, null, maxCalculatingErrorDivisor);
        if (!s.solvingIsRight) {
            callback.onMatrixSolved(res.getString(R.string.calc_err) + " $$" + TeXConverter.BigFractionWithBigDecimalToString(s.getCurrentCalculatingError(), digits_sv, roundingMode_sv) + "$$" + res.getString(R.string.is_bigger_than) +
                    " $$" + TeXConverter.BigFractionWithBigDecimalToString(s.getMaxCalculatingError(), digits_sv, roundingMode_sv) + "$$" + "<br>", solver.getSolvingLog());
            return;
        } else {
            rootsTeX += res.getString(R.string.divergence) + ": $$" + TeXConverter.BigFractionWithBigDecimalToString(s.getCurrentCalculatingError(), digits_sv, roundingMode_sv) + "$$<br>";
        }
        if (callback != null) callback.onMatrixSolved(rootsTeX, solver.getSolvingLog());
    }


    public interface OnSolveCallback {
        void onMatrixSolved(String answerTeX, String solutionTeX);
    }
}

