package math.matrixsolver.matrix.solver.methods;

import android.content.Context;
import android.content.res.Resources;

import com.github.kiprobinson.bigfraction.BigFraction;

import math.matrixsolver.R;
import math.matrixsolver.matrix.Matrix;
import math.matrixsolver.matrix.TeXConverter;
import math.matrixsolver.matrix.solver.Solver;

public class GaussSolver extends Solver {

    private Matrix matrixObject;

    public GaussSolver(Context context) {
        super(context);
    }

    private void checkStringForZeroes(BigFraction[] string) throws InfinityRootsException, NoRootsException {
        for (int i = 0; i < string.length - 1; i++)
            if (!(string[i].equals(BigFraction.ZERO)))
                return; // Normal string, continuing maxIterations

        if (string[string.length - 1].equals(BigFraction.ZERO))
            throw new InfinityRootsException();
        else throw new NoRootsException();
    }

    public BigFraction[] solveMatrix(Matrix matrixObject) throws InfinityRootsException, NoRootsException {

        Resources res = context.getResources();
        this.matrixObject = matrixObject.clone();
        BigFraction[][] matrix = this.matrixObject.getMatrix();
        sb = new StringBuilder();

        sb.append("<h><center>").append(res.getString(R.string.solvingsteps_gauss_title)).append("</center></h>").append(newline).append(newline);
        // Apply matrix to triangular view
        BigFraction k;
        BigFraction[] temp;
        int b = 0;
        while (b < matrix.length) {
            if (b + 1 < matrix.length)
                sb.append("<h><center>").append(res.getString(R.string.solvingsteps_iteration)).append(" №").append(b + 1).append(":").append("</center></h>").append(newline);
            for (int i = b; i < matrix.length - 1; i++) {
                sb.append("<i>").append(res.getString(R.string.solvingsteps_select_string)).append(" #").append(i + 1).append(":").append("</i>").append(newline);
                k = getCoefficient(b, i + 1, b);
                sb.append(res.getString(R.string.solvingsteps_calc_coeff)).append(": ");
                sb.append("$$k=").append(TeXConverter.BigFractionToTeX(k)).append("$$").append(newline);
                sb.append(res.getString(R.string.solvingsteps_mul_str_by_coeff)).append(": ")
                        .append(TeXConverter.arrayToTex(this.matrixObject.getString(b))).append("<center>↓</center>");

                temp = this.matrixObject.multiplyString(b, k);

                sb.append(TeXConverter.arrayToTex(this.matrixObject.getString(b)));
                this.matrixObject.setString(Matrix.subtractStrings(this.matrixObject.getString(i + 1), temp), i + 1);
                checkStringForZeroes(this.matrixObject.getString(i + 1));
            }
            b++;
        }
        sb.append(res.getString(R.string.solvingsteps_matrix_trapezional)).append(":").append(newline);
        sb.append(TeXConverter.matrixToTeX(this.matrixObject));
        sb.append(newline);
        sb.append("<h><center>").append(res.getString(R.string.solvingsteps_finding_roots)).append("</center></h>").append(newline);

        BigFraction[] roots = new BigFraction[matrix[0].length - 1];
        BigFraction[] temp2;
        for (int u = 0; u < roots.length; u++) {
            if (u != 0)
                sb.append(res.getString(R.string.solvingsteps_shrink_str)).append(" #").append(u + 1).append(" ").append(res.getString(R.string.solvingsteps_by_known_roots)).append(": ");
            temp2 = this.matrixObject.getString(matrix.length - 1 - u);
            if (u != 0)
                sb.append(TeXConverter.arrayToTex(temp2)).append("<center>↓</center>");
            for (int i = 0; i < u; i++) {
                temp2[temp2.length - 1] = temp2[temp2.length - 1].subtract(roots[roots.length - 1 - i].multiply(temp2[temp2.length - 2 - i]));
                temp2[temp2.length - 2 - i] = BigFraction.ZERO;
            }
            if (u != 0)
                sb.append(TeXConverter.arrayToTex(temp2)).append(newline);
            roots[roots.length - 1 - u] = temp2[temp2.length - 1].divide(temp2[temp2.length - 1 - 1 - u]);
            sb.append(res.getString(R.string.solvingsteps_root)).append(" #").append(roots.length - u).append(" ").append(res.getString(R.string.solvingsteps_is_found)).append(":")
                    .append("$$x_").append(roots.length - u).append("=").append(TeXConverter.BigFractionToTeX(roots[roots.length - 1 - u])).append("$$");
        }
        return roots;
    }

    private BigFraction getCoefficient(int posOfFirstString, int posOfSecondString, int bias) {
        if (((posOfFirstString > this.matrixObject.getMatrix().length) || (posOfFirstString < 0)) ||
                ((posOfSecondString > this.matrixObject.getMatrix().length) || (posOfSecondString < 0)) ||
                (bias > this.matrixObject.getMatrix()[posOfFirstString].length)) {
            throw new ArrayIndexOutOfBoundsException("Some parameters invalid with this matrix");
        }
        if (this.matrixObject.getString(posOfFirstString)[bias].equals(BigFraction.ZERO)) {
            throw new ArithmeticException("Dividing by zero");
        }
        return this.matrixObject.getString(posOfSecondString)[bias].divide(this.matrixObject.getString(posOfFirstString)[bias]);
    }
}
