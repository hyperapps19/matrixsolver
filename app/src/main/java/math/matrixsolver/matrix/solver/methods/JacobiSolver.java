package math.matrixsolver.matrix.solver.methods;

import android.content.Context;
import android.content.res.Resources;

import com.github.kiprobinson.bigfraction.BigFraction;

import math.matrixsolver.R;
import math.matrixsolver.matrix.TeXConverter;
import math.matrixsolver.matrix.solver.IterationSolver;

public class JacobiSolver extends IterationSolver {


    public JacobiSolver(Context context) {
        super(context);
    }

    public BigFraction[] doIterations(BigFraction[][] matrix, BigFraction[] sourceRoots, BigFraction iterationPrecision, int maxIterations) {
        Resources res = context.getResources();
        BigFraction[] roots = sourceRoots.clone();
        BigFraction[] roots2 = roots.clone();
        boolean iterate = true;
        int k = 0;
        while (iterate && k < maxIterations) {
            sb.append(res.getString(R.string.solvingsteps_iteration)).append(" №")
                    .append(k + 1).append(":").append(newline);
            for (int i = 0; i < matrix.length; i++) {
                roots[i] = matrix[i][matrix[i].length - 1];
                for (int j = 0; j < matrix[i].length - 1; j++)
                    if (i != j) roots[i] = sourceRoots[i].subtract(roots[j].multiply(matrix[i][j]));
                roots[i] = sourceRoots[i].divide(matrix[i][i]);

                for (int b = 0; b < roots.length; b++) {
                    iterate = false;
                    if ((roots[b].subtract(roots2[b]).abs().compareTo(iterationPrecision)) > 0) {
                        iterate = true;
                        break;
                    }
                }
            }
            sourceRoots = roots;
            sb.append(res.getString(R.string.solvingsteps_roots_closer))
                    .append(TeXConverter.arrayToTex(roots)).append(newline);
            k++;
            roots2 = roots.clone();
        }
        return roots;
    }

}