package math.matrixsolver.matrix;

import com.github.kiprobinson.bigfraction.BigFraction;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;

public class TeXConverter {
    private TeXConverter() {
    } // Prohibit to instantiate

    public static String arrayToTex(BigFraction[] arr) {
        StringBuilder sb = new StringBuilder();
        sb.append("\\[\\begin{bmatrix}");
        for (BigFraction num : arr) {
            sb.append(BigFractionToTeX(num));
            if (!num.equals(arr[arr.length - 1])) sb.append("&");
        }
        sb.append("\\\\");
        sb.append("\\end{bmatrix}\\]");
        return sb.toString();
    }

    public static String matrixToTeX(Matrix matrix) {
        StringBuilder sb = new StringBuilder();
        sb.append("\\[\\begin{bmatrix}");
        for (BigFraction[] str : matrix.getMatrix()) {
            for (BigFraction num : str) {
                sb.append(BigFractionToTeX(num));
                if (!num.equals(str[str.length - 1])) sb.append("&");
            }
            sb.append("\\\\");
        }
        sb.append("\\end{bmatrix}\\]");
        return sb.toString();
    }

    public static String rootsToTeX(BigFraction[] rootsArray, int scale, RoundingMode roundingMode) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < rootsArray.length; i++)
            sb.append("$$").append("x_{").append(i + 1).append("}")
                    .append(BigFractionWithBigDecimalToString(rootsArray[i], scale, roundingMode))
                    .append("$$").append(System.lineSeparator());
        return sb.toString();
    }

    public static String BigFractionToTeX(BigFraction bf) {
        StringBuilder sb = new StringBuilder();
        boolean noFrac = false;
        BigFraction bf1;
        if (bf.compareTo(BigFraction.ZERO) < 0) {
            sb.append("-");
            bf1 = bf.abs();
        } else bf1 = bf;
        if (bf1.getDenominator().equals(BigInteger.ONE)) {
            sb.append(bf1.getNumerator());
            noFrac = true;
        } else if (bf1.getIntegerPart().compareTo(BigInteger.ZERO) != 0) {
            sb.append(bf1.getIntegerPart());
            bf1 = bf1.subtract(bf1.getIntegerPart());
        }
        if ((bf1.compareTo(BigFraction.ZERO) != 0) && (!noFrac))
            sb.append("\\frac{").append(bf1.getNumerator()).append("}{").append(bf1.getDenominator()).append("}");

        return sb.toString();
    }

    public static String[] matrixToSLAE(Matrix matrix) {
        String[] ret = new String[matrix.getMatrix().length];
        StringBuilder temp = new StringBuilder();
        for (int i = 0; i < matrix.getMatrix().length; i++) {
            for (int j = 0; j < matrix.getString(0).length; j++) {
                if (matrix.getString(i)[j].compareTo(BigFraction.ZERO) != 0 || (j == matrix.getString(0).length - 1)) {
                    if (j == matrix.getString(0).length - 1) temp.append("=");
                    else if (j != 0 && (matrix.getString(i)[j]).compareTo(BigFraction.ZERO) >= 0)
                        if ((matrix.getString(i)[j - 1]).compareTo(BigFraction.ZERO) != 0)
                            temp.append("+");
                    if (matrix.getString(i)[j].compareTo(BigFraction.ONE) != 0)
                        temp.append(BigFractionToTeX(matrix.getString(i)[j]));
                    if (j != matrix.getString(0).length - 1)
                        temp.append("x_").append(j + 1).append(" ");
                }
            }
            ret[i] = temp.toString();
            temp.setLength(0);
        }
        return ret;
    }

    public static String SLAEToTeX(String[] SLAE) {
        StringBuilder sb = new StringBuilder();
        sb.append("$$").append("\\begin{cases}\n");
        for (String SLAEElement : SLAE) {
            sb.append(SLAEElement).append("\n");
            if (!SLAEElement.equals(SLAE[SLAE.length - 1])) sb.append("\\\\\n");
        }
        sb.append("\\end{cases}\n").append("$$");

        return sb.toString();
    }

    public static BigDecimal BigFractionToBigDecimal(BigFraction bf, int scale, RoundingMode roundingMode) {
        return (new BigDecimal(bf.getNumerator())
                .divide(new BigDecimal(bf.getDenominator()), scale, roundingMode))
                .stripTrailingZeros();
    }

    public static String BigFractionWithBigDecimalToString(BigFraction bf, int scale, RoundingMode roundingMode) {
        if (bf.equals(BigFraction.ZERO) || bf.getDenominator().equals(BigInteger.ONE))
            return "=" + TeXConverter.BigFractionToTeX(bf);
        String s = TeXConverter.BigFractionToBigDecimal(bf, scale, roundingMode).toPlainString();
        // Remove leading and trailing zeroes
        s = !s.contains(".") ? s : s.replaceAll("0*$", "").replaceAll("\\.$", "");
        return "≈" + s + "=\\:(" + TeXConverter.BigFractionToTeX(bf) + ")";
    }
}
