package math.matrixsolver.matrix;

import com.github.kiprobinson.bigfraction.BigFraction;

import java.util.Random;

public class MatrixGenerator {

    static private double newRandomNumber(double min, double max, boolean generateFractionalNumbers) {
        Random random = new Random();
        if (generateFractionalNumbers)
            return min + (max - min) * random.nextDouble(); // Random fractional number
        else return random.nextInt((int) ((max - min) + 1)) + min; // Random decimal number
    }

    static private BigFraction[] generateString(double root, int x, double min, double max, boolean generateFractionalNumbers) {
        BigFraction[] string = new BigFraction[x];
        string[string.length - 1] = BigFraction.ZERO;
        for (int i = 0; i < x - 1; i++) {
            string[i] = BigFraction.valueOf(newRandomNumber(min, max, generateFractionalNumbers));
            string[string.length - 1] = string[string.length - 1].add(BigFraction.valueOf(root).multiply(string[i]));
        }
        return string;
    }

    public static double[] generateRoots(int rootsNumber, double min, double max, boolean generateFractionalNumbers) {
        double[] roots = new double[rootsNumber];
        for (int i = 0; i < rootsNumber; i++)
            roots[i] = newRandomNumber(min, max, generateFractionalNumbers);
        return roots;
    }

    public static BigFraction[][] generateNewMatrix(double[] roots, double min, double max, boolean generateFractionalNumbers) {
        BigFraction[][] matrix = new BigFraction[roots.length][roots.length + 1];
        for (int i = 0; i < matrix.length; i++)
            matrix[i] = generateString(roots[i], roots.length + 1, min, max, generateFractionalNumbers);
        return matrix;
    }
}
