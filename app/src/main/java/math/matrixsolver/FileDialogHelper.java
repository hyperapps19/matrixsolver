package math.matrixsolver;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;

import com.rustamg.filedialogs.FileDialog;
import com.rustamg.filedialogs.OpenFileDialog;
import com.rustamg.filedialogs.SaveFileDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import math.matrixsolver.ui.activities.ActivityEssential;

public class FileDialogHelper {
    public static final boolean TYPE_OPEN_FILE = false;
    public static final boolean TYPE_SAVE_FILE = true;

    public static FileDialog initFileDialog(AppCompatActivity activity, boolean type) {
        if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED)
            Toast.makeText(activity, R.string.request_read, Toast.LENGTH_SHORT).show();
        else {
            FileDialog fileDialog;
            if (type == TYPE_OPEN_FILE) fileDialog = new OpenFileDialog();
            else fileDialog = new SaveFileDialog();
            if (ActivityEssential.getTheme(activity).equals("dark"))
                fileDialog.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.DarkTheme);
            else fileDialog.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.AppTheme);
            fileDialog.show(activity.getSupportFragmentManager(), OpenFileDialog.class.getName());
            return fileDialog;
        }
        return null;
    }

    public static byte[] readFileToBytes(File file) throws IOException {
        ByteArrayOutputStream ous = null;
        InputStream ios = null;
        try {
            byte[] buffer = new byte[4096];
            ous = new ByteArrayOutputStream();
            ios = new FileInputStream(file);
            int read;
            while ((read = ios.read(buffer)) != -1) {
                ous.write(buffer, 0, read);
            }
        } finally {
            try {
                if (ous != null)
                    ous.close();
            } catch (IOException e) {
                e.printStackTrace();
            }

            try {
                if (ios != null)
                    ios.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return ous.toByteArray();
    }

    public static boolean checkIfFileCanBeSaved(Context context) {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(context, R.string.request_write, Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public static String addExtensionIfNeeded(String baseFName, String extension) {
        if (!baseFName.endsWith(extension)) return baseFName + extension;
        return baseFName;
    }

    public static void saveFileFromBytes(File file, Context context, byte[] content) {
        if (checkIfFileCanBeSaved(context)) {
            try {
                FileOutputStream outputStream = new FileOutputStream(addExtensionIfNeeded(file.toString(), ".matrix"));
                outputStream.write(content);
                outputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static void showOperatingWithFilesError(Context context) {
        Toast.makeText(context, R.string.err_oper_file, Toast.LENGTH_SHORT).show();
    }
}
