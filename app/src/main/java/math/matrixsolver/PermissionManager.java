package math.matrixsolver;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

public class PermissionManager {
    public static final int PERMISSION_ALREADY_GRANTED = 0;
    public static final int NEED_TO_ASK_FOR_PERMISSION = 1;
    public static final int USER_DOESNT_WANT_TO_GIVE_PERMISSION = 2;

    private PermissionManager() {
    }

    public static int checkIfNeedToRequestPermission(Activity activity, String permission) {
        if (ContextCompat.checkSelfPermission(activity, permission)
                != PackageManager.PERMISSION_GRANTED)
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity, Manifest.permission.READ_CONTACTS))
                return USER_DOESNT_WANT_TO_GIVE_PERMISSION;
            else return NEED_TO_ASK_FOR_PERMISSION;
        else return PERMISSION_ALREADY_GRANTED;
    }
}